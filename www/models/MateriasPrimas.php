<?php
class MateriasPrimas extends ActiveRecord\Model
{
    static $table_name = 'materias_primas';    
    static $primary_key = 'id_mat_pri';    
    static $validaciones = Array(
        "des_mat_pri" => Array(
            "nombre" => "descripción de la materia prima",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function listarMateriasPrimass()
    {          
        $data = MateriasPrimas::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarMateriasPrimas($id)
    {       
        $tip_arm = MateriasPrimas::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarMateriasPrimas($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , MateriasPrimas::$validaciones);
        if($respuesta === true)
        {
            if($id_mat_pri = MateriasPrimas::create($datos))
            {
                return Herramientas::obtener_atributos($id_mat_pri)->id_mat_pri;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarMateriasPrimas($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , MateriasPrimas::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = MateriasPrimas::find($datos["id_mat_pri"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarMateriasPrimas($id)
    {             
        try 
        {
            $tip_arm = MateriasPrimas::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
