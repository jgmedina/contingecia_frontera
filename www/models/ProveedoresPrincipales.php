<?php
class ProveedoresPrincipales extends ActiveRecord\Model
{
    static $table_name = 'proveedores_principales';    
    static $primary_key = 'id_proo_pri';    
    static $validaciones = Array(
        "des_proo_pri" => Array(
            "nombre" => "descripción del proveedor principal",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function listarProveedoresPrincipaless()
    {          
        $data = ProveedoresPrincipales::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarProveedoresPrincipales($id)
    {       
        $tip_arm = ProveedoresPrincipales::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarProveedoresPrincipales($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , ProveedoresPrincipales::$validaciones);
        if($respuesta === true)
        {
            if($id_proo_pri = ProveedoresPrincipales::create($datos))
            {
                return Herramientas::obtener_atributos($id_proo_pri)->id_proo_pri;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarProveedoresPrincipales($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , ProveedoresPrincipales::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = ProveedoresPrincipales::find($datos["id_proo_pri"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarProveedoresPrincipales($id)
    {             
        try 
        {
            $tip_arm = ProveedoresPrincipales::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
