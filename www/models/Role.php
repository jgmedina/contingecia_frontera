<?php
class Role extends ActiveRecord\Model
{
    static $primary_key = 'id';
    
    static $has_many = array(
      array('roles_usuarios'),
      array('usuarios', 'through' => 'roles_usuarios')
    );
    
    public function listarRoles()
    {
        $datos = Role::find('all',array('order' => 'nom_rol asc'));
        $roles = Array();
        foreach ($datos as $rol) 
        {
            array_push($roles, (object)$rol->attributes());
        }
        
        return (object)$roles; 
    }
    
    public function consultarRol($id_rol)
    {
        $rol = Role::find($id_rol);
        /*
        $sql = "SELECT 
                    * --id_rol , nom_rol
                FROM 
                    roles
                WHERE
                    id_rol = $id_rol";
        
        return $this->_db->consultar($sql , TRUE);
        */
        return (object)$rol->attributes();
    }
    
    public function consultarPermisosRoles($id_rol)
    {
        $sql = "SELECT 
                    id_per, 
                    fk_per, 
                    nom_per,
                    r.id,
                    nom_rol,                    
                    coalesce((SELECT val_per_rol FROM permisos_roles WHERE id_per = p.id_per AND id_rol = $id_rol LIMIT 1)::integer , 0) AS estado
                FROM 
                    permisos p,
                    roles r
                WHERE 
                    r.id = $id_rol
                AND 
                    nom_per <> 'index'
                ORDER BY 
                    fk_per, 
                    nom_rol";
        
        $roles = Array();
        foreach (Role::find_by_sql($sql) as $rol) 
        {
            array_push($roles, (object)$rol->attributes());
        }
        
        return (object)$roles; 
    }
    
    public function agregarRol($data)
    {
        $existe = Role::find_by_nom_rol($data["nom_rol"]);
        
        if(!$existe)
        {
            $resultado = (object)Role::create(Array('nom_rol'=> $data["nom_rol"]));        
            $resultado = (object)$resultado->attributes();
            
            if($id_rol = $resultado->id)
            {            
                return $id_rol;             
            }
            else
            {            
                return false; 
            }
        }
        else
        {            
            return -1; 
        }
    }
    
    public function modificarRol($id_rol , $nom_rol)
    {
        $data = Array(
                    "id_rol"=>$id_rol,
                    "nom_rol"=>$nom_rol
                    );
        
        if($this->_db->editar("roles", $data, "id_rol"))        
        {                        
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function eliminarRol($id_rol)
    {
        $data = RolesUsuario::consultarRolesUsuarios($id_rol);
        if(count($data) == 0)
        {
            try 
            {
                $rol = Role::find($id_rol);
                $rol->delete();
            }
            catch (ActiveRecord\RecordNotFound $e) 
            {
                return FALSE;
                exit();
            }

            return TRUE;
        }
        else
        {
            return -1;
        }
    }  
}
?>
