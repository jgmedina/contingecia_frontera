<?php
class Empresa extends ActiveRecord\Model
{
    static $table_name = 'empresas';    
    static $primary_key = 'id_emp';    
    static $validaciones = Array(
        "raz_soc_emp" => Array(
            "nombre" => "Razón Social",
            "longitud" => "27",
            "NULL" => false
        ),
        "rif_emp" => Array(
            "nombre" => "R.I.F.",
            "longitud" => "10",
            "NULL" => false
        ),
        "tip_emp" => Array(
            "nombre" => "Tipo de Empresa",
            "longitud" => "1",
            "NULL" => false
        ),
        "id_pro" => Array(
            "nombre" => "Identificador de la tabla propietario",
            "NULL" => false
        ),
        "id_pro_ter" => Array(
            "nombre" => "Identificador de la tabla propietario del terreno",
            "NULL" => false
        ),
        "id_pro_inf" => Array(
            "nombre" => "Identificador de la tabla propietario de la infraestructura",
            "NULL" => false
        )
    );
    
    public function listarEmpresas()
    {          
        $data = Empresa::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarEmpresa($id)
    {       
        $tip_arm = Empresa::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarEmpresa($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Empresa::$validaciones);
        if($respuesta === true)
        {
            if(!Empresa::find_by_rif_emp($datos["rif_emp"]))
            {
                if($id_emp = Empresa::create($datos))
                {
                    return Herramientas::obtener_atributos($id_emp)->id_emp;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarEmpresa($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Empresa::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = Empresa::find($datos["id_emp"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarEmpresa($id)
    {             
        try 
        {
            $tip_arm = Empresa::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
