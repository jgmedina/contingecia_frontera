<?php
class Validaciones extends ActiveRecord\Model
{    
    static $connection = "app";
    static $table_name = 'validaciones';    
    static $primary_key = 'id_val';    
    
    public function agregarValidaciones($controlador , $metodo, $campo , $valor , $msg , $tipo)
    {               
        ValidadoresCampo::connection()->transaction();
        $data = Array(                    
                    'id_campo'=> OrigenCampo::agregarCampos($controlador , $metodo, $campo)->id,                      
                    'id_val'=> Validaciones::find_by_cod_att_val($tipo)->id_val, 
                    'val_att_val_cam'=> stripcslashes($valor),
                    "val_msg_val_cam"=>$msg
                    );
        
        if(ValidadoresCampo::agregarValidacionesCampos($data) && $campo)
        {            
            ValidadoresCampo::connection()->commit();
            return  TRUE;
        }
        else
        {
            ValidadoresCampo::connection()->rollback();
            return  FALSE;
        }
    }    
}
?>
