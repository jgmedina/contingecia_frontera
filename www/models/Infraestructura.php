<?php
class Infraestructura extends ActiveRecord\Model
{
    static $table_name = 'infraestructuras';    
    static $primary_key = 'id_inf';    
    static $validaciones = Array(
        "des_inf" => Array(
            "nombre" => "Descripción de Infraestructura",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function listarInfraestructuras()
    {          
        $data = Infraestructura::find('all');
        return (array)Herramientas::obtener_atributos($data);
    }
    
    public function consultarInfraestructura($id)
    {       
        $tip_arm = Infraestructura::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarInfraestructura($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Infraestructura::$validaciones);
        if($respuesta === true)
        {
            if(!Infraestructura::find_by_nom_tip_arm($datos["nom_tip_arm"]))
            {
                Infraestructura::create($datos);
                if($id_inf = Infraestructura::find_by_nom_tip_arm($datos["nom_tip_arm"])->id_inf)
                {
                    return $id_inf;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarInfraestructura($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Infraestructura::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = Infraestructura::find($datos["id_inf"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarInfraestructura($id)
    {             
        try 
        {
            $tip_arm = Infraestructura::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
