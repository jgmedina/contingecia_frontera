<?php
class OrigenCampo extends ActiveRecord\Model
{
    static $connection = "app";
    
    public function consultarCampoControladorMetodo($campo , $controlador , $metodo)
    {
        $sql = "SELECT 		
		v.cod_att_val ,                    
		coalesce(val_att_val_cam ,'NULL' ) AS val_att_val_cam ,                    
		v.cod_msg_val ,
		coalesce(val_msg_val_cam ,'NULL' ) AS val_msg_val_cam                                        
	FROM 
		origen_campos oc
	LEFT JOIN 
		validadores_campos vc ON oc.id = vc.id_campo
	LEFT JOIN 
		validaciones v ON vc.id_val = v.id_val
	WHERE 
		nombre_campo = '$campo'  
        AND
		controlador = '$controlador'
	AND 
		metodo =  '$metodo' ";
        //Herramientas::debug($sql);
        return OrigenCampo::find_by_sql($sql);
    }       
    
    public function consultarCampo($campo)
    {
        $sql = "SELECT 		
		v.cod_att_val ,                    
		coalesce(val_att_val_cam ,'NULL' ) AS val_att_val_cam ,                    
		v.cod_msg_val ,
		coalesce(val_msg_val_cam ,'NULL' ) AS val_msg_val_cam                                        
	FROM 
		origen_campos oc
	LEFT JOIN 
		validadores_campos vc ON oc.id = vc.id_campo
	LEFT JOIN 
		validaciones v ON vc.id_val = v.id_val
	WHERE 
		nombre_campo = '$campo'";
        
        return OrigenCampo::find_by_sql($sql);
    }       

    public function consultarTipoCampo($campo)
    {
        $campo = (object)OrigenCampo::find_by_nombre_campo($campo);
        return $campo;
    }     
    
    public function agregarCampos($controlador , $metodo, $nombre_campo)
    {        
        if(!$campo = OrigenCampo::find_by_nombre_campo_and_controlador_and_metodo($nombre_campo,$controlador , $metodo))
        {
            $attributes = Array(                    
                    'nombre_campo'=> $nombre_campo, 
                    'controlador'=> $controlador, 
                    'metodo'=> $metodo
                    );                    

            if($campo = (object)OrigenCampo::create($attributes))
            {
                return  $campo;
            }
            else
            {
                return  FALSE;
            }
        }
        else 
        {
            return $campo;
        }
    }

    public function eliminarCampos($controlador , $metodo)
    {        
        $campos = OrigenCampo::find_by_controlador_and_metodo($controlador, $metodo);        
        if($campos)
        {
            $respuesta = ValidadoresCampo::eliminarValidacionesCampo($campos);
            if($campos && $respuesta)
            {            
                if($campos->delete())
                {
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return TRUE;
            }
        }
        else
        {
            return TRUE;
        }
    }
    
    
}
?>
