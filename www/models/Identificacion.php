<?php
class Identificacion extends ActiveRecord\Model
{
    static $table_name = 'identificaciones';    
    static $primary_key = 'id_ide';    
    static $validaciones = Array(
        "fec_ide" => Array(
            "nombre" => "fecha de la sección I Identificación",
            "longitud" => "10",
            "NULL" => false
        ),
        "num_pla_ide" => Array(
            "nombre" => "número de la planilla",
            "longitud" => "15",
            "NULL" => false
        ),
        "est_ide" => Array(
            "nombre" => "estado de la unidad económica",
            "longitud" => "1"
        ),
        "tel_ide" => Array(
            "nombre" => "teléfono de la secciópn I Identificación",
            "longitud" => "12"
        ),
        "dir_ide" => Array(
            "nombre" => "dirección",
            "longitud" => "255"
        )
    );
    
    public function agregarIdentificacion($datos)
    {        
        $datos["fec_ide"] = Herramientas::fechaPostgres($datos["fec_ide"]);
        $respuesta = Herramientas::validaciones($datos , Identificacion::$validaciones);                
        if($respuesta === true)
        {
            if(!Identificacion::find_by_num_pla_ide($datos["num_pla_ide"]))
            {
                if($id_ide = Identificacion::create($datos))
                {
                    return Herramientas::obtener_atributos($id_ide)->id_ide;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
