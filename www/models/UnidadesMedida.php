<?php
class UnidadesMedida extends ActiveRecord\Model
{
    static $table_name = 'unidades_medidas';    
    static $primary_key = 'id_uni_med';    
    static $validaciones = Array(
        "gru_uni_med" => Array(
            "nombre" => "Grupo de la unidad de medida",
            "longitud" => "55",
            "NULL" => false
        ),
        "nom_uni_med" => Array(
            "nombre" => "Nombre de la unidad de medida",
            "longitud" => "55",
            "NULL" => false
        ),
        "sim_uni_med" => Array(
            "nombre" => "Simbolo de la unidad de medida",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function listarUnidadesMedidas()
    {                  
        $data = UnidadesMedida::all(Array("group"=>"gru_uni_med,nom_uni_med,sim_uni_med,id_uni_med","order"=>"nom_uni_med"));
        return (array)Herramientas::obtener_atributos($data);
    }
    
    public function listarGruposUnidadesMedidas()
    {                  
        $data = UnidadesMedida::find_by_sql("SELECT DISTINCT ON (gru_uni_med) * FROM unidades_medidas ORDER BY gru_uni_med");
        return (array)Herramientas::obtener_atributos($data);
    }
    
    public function consultarUnidadesMedidas($id_uni_med)
    {        
        return Herramientas::obtener_atributos(UnidadesMedida::find($id_uni_med));
    }
    
    public function agregarUnidadesMedidas($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , UnidadesMedida::$validaciones);
        if($respuesta === true)
        {
            UnidadesMedida::connection()->transaction();
            if(!UnidadesMedida::find_by_nom_uni_med_or_sim_uni_med($datos["nom_uni_med"],$datos["sim_uni_med"]))
            {
                if($id_uni_med = UnidadesMedida::create($datos)->id_uni_med)
                {
                    UnidadesMedida::connection()->commit();
                    return true;
                }
                else
                {
                    UnidadesMedida::connection()->rollback();
                    return false;
                }
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarUnidadesMedidas($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , UnidadesMedida::$validaciones);
        if($respuesta === true)
        {
            UnidadesMedida::connection()->transaction();
            $medida = UnidadesMedida::find($datos["id_uni_med"]);
            
            if($medida->update_attributes($datos))
            {                
                UnidadesMedida::connection()->commit();
                return true;
            }
            else
            {
                UnidadesMedida::connection()->rollback();
                return false;
            }
        }
        else 
        {
            return $respuesta;
        }
    }
    
    public function eliminarUnidadesMedidas($id_uni_med)
    {             
        try 
        {
            UnidadesMedida::connection()->transaction();
            $medida = UnidadesMedida::find($id_uni_med);
            
            if(!$medida->delete())
            {
                UnidadesMedida::connection()->rollback();
                return FALSE;
            }
            else
            {
                UnidadesMedida::connection()->commit();
                return TRUE;
            }
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            UnidadesMedida::connection()->rollback();
            return FALSE;
            exit();
        }
    }          
}
?>
