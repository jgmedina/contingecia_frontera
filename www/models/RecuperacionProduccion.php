<?php
class RecuperacionProduccion extends ActiveRecord\Model
{
    static $table_name = 'recuperacion_produccion';    
    static $primary_key = 'id_rec_pro';    
    static $validaciones = Array(
        "id_uni_eco" => Array(
            "nombre" => "Identificador de la tabla unidad economica",
            "NULL" => false
	),
        "lab_rec_pro" => Array(
            "nombre" => "¿Laboral?",
            "NULL" => false
        ),	
        "lab_exp_rec_pro" => Array(	
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),
        "lab_sol_rec_pro" => Array(	
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "log_rec_pro" => Array(
            "nombre" => "¿Logísticos?",
            "NULL" => false
            ),
        "log_exp_rec_pro" => Array(	
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),
        "log_sol_rec_pro" => Array(
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "dis_rec_pro" => Array(
            "nombre" => "¿Distribución?",
        ),	
        "dis_exp_rec_pro" => Array(	
            "nombre" => "Explicar problema",
            "longitud" => "255",
        ),
        "dis_sol_rec_pro" => Array(	
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "leg_rec_pro" => Array(
            "nombre" => "¿Legales?",
            "NULL" => false
        ),	
        "leg_exp_rec_pro" => Array(
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),		
        "leg_sol_rec_pro" => Array(
            "nombre" => "Propuesta de Solución"
            ,"longitud" => "255"
        ),    
        "mat_pri_rec_pro" => Array(
            "nombre" => "¿Materia Prima?",
            "NULL" => false
        ),                
        "deu_pro_rec_pro" => Array(
            "nombre" => "¿Deuda con los Proveedores Prima?"
        ),
        "deu_pro_mon_rec_pro" => Array(
            "nombre" => "Monto"
        ),
        "fal_mat_rec_pro" => Array(
            "nombre" => "¿Falta de materia para la producción?"
        ),
        "esc_ins_rec_pro" => Array(
            "nombre" => "¿Escasez de Insumo?"
        ),
        "fal_div_rec_pro" => Array(
            "nombre" => "¿Falta de Divisas?"
        ),
        "ret_cnp_rec_pro" => Array(
            "nombre" => "¿Retraso en CNP?"
        ),
        "otr_rec_pro" => Array(
            "nombre" => "Otros",
            "longitud" => "55"
        ),
        "mat_pri_exp_rec_pro" => Array(
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),
        "mat_pri_sol_rec_pro" => Array(
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "maq_rec_pro" => Array(
            "nombre" => "¿Maquinaria?",
            "NULL" => false
        ),
        "maq_obs_rec_pro" => Array(
            "nombre" => "¿Obsolescencia?"
        ),   
        "maq_rep_mon_rec_pro" => Array(
            "nombre" => "¿Falta de repuestos?"
        ),
        "maq_man_rec_pro" => Array(
            "nombre" => "¿Mantenimiento?"
        ),
        "maq_adu_rec_pro" => Array(
            "nombre" => "¿Problema Aduanal?"
        ),
        "maq_otr_rec_pro" => Array(
            "nombre" => "Otros",
            "longitud" => "55"
        ),
        "maq_pri_exp_rec_pro" => Array(
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),
        "maq_pri_sol_rec_pro" => Array(
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "asi_tec_rec_pro" => Array(
            "nombre" => "¿Asistencia Técnica?",
            "NULL" => false
        ),
        "rec_fin_rec_pro" => Array(
            "nombre" => "¿Falta de recursos Financieros?"
        ),
        "rec_fin_mon_rec_pro" => Array(
            "nombre" => "Monto",
        ),
        "fal_asi_tec_rec_pro" => Array(
            "nombre" => "¿Falta de Asistencia?"
        ),
        "asi_tec_exp_rec_pro" => Array(
            "nombre" => "Explicar problema",
            "longitud" => "255"
        ),
        "asi_tec_sol_rec_pro" => Array(
            "nombre" => "Propuesta de Solución",
            "longitud" => "255"
        ),
        "maq_fal_div_rec_pro" => Array(
            "nombre" => "¿Falta de Divisas?",
        ),
        "maq_fal_div_mon_rec_pro" => Array(
            "nombre" => "Monto"
        )
    );
    
    public function agregarRecuperacionProduccion($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , RecuperacionProduccion::$validaciones);
        if($respuesta === true)
        {
            if(!RecuperacionProduccion::find_by_rif_emp($datos["rif_emp"]))
            {
                if($id_rec_pro = RecuperacionProduccion::create($datos))
                {
                    return Herramientas::obtener_atributos($id_rec_pro)->id_rec_pro;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
