<?php
class PropietarioInfraestructura extends ActiveRecord\Model
{
    static $table_name = 'propietario_infraestructura';    
    static $primary_key = 'id_pro_inf';    
    static $validaciones = Array(
        "tip_pro_inf" => Array(
            "nombre" => "Tipo de propietario de la Infraestructura",
            "longitud" => "10",
            "NULL" => false
        ),
        "raz_nom_pro_inf" => Array(
            "nombre" => "Nombre o Razón Social",
            "longitud" => "155",
            "NULL" => false
        ),
        "tip_doc_pro_inf" => Array(
            "nombre" => "Tipo de documento de identidad",
            "longitud" => "1",
            "NULL" => false
        ),
        "doc_ide_pro_inf" => Array(
            "nombre" => "Número de documento de identidad",
            "longitud" => "10",
            "NULL" => false
        )
    );
    
    public function listarPropietarioInfraestructuras()
    {          
        $data = PropietarioInfraestructura::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarPropietarioInfraestructura($id)
    {       
        $tip_arm = PropietarioInfraestructura::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarPropietarioInfraestructura($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , PropietarioInfraestructura::$validaciones);
        if($respuesta === true)
        {
            if(!PropietarioInfraestructura::find_by_doc_ide_pro_inf($datos["doc_ide_pro_inf"]))
            {
                if($id_pro_inf = PropietarioInfraestructura::create($datos))
                {
                    return Herramientas::obtener_atributos($id_pro_inf)->id_pro_inf;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarPropietarioInfraestructura($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , PropietarioInfraestructura::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = PropietarioInfraestructura::find($datos["id_pro_inf"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarPropietarioInfraestructura($id)
    {             
        try 
        {
            $tip_arm = PropietarioInfraestructura::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
