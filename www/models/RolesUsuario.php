<?php
class RolesUsuario extends ActiveRecord\Model
{
    //static $primary_key = 'id_rol';
    
    static $belongs_to = array(
                            array('usuario'),
                            array('role')
       );
    
    public function consultarRolesUsuarios($id_rol)
    {
        return RolesUsuario::all(array("conditions"=>"role_id = $id_rol"));
    }
}
?>
