<?php
class ReferenciasCartograficas extends ActiveRecord\Model
{
    static $table_name = 'referencias_cartograficas';    
    static $primary_key = 'id_ref_car';    
    static $validaciones = Array(
        "fot_1_ref_car" => Array(
            "nombre" => "fotográfia número 1",
            "longitud" => "55"
        ),
        "fot_2_ref_car" => Array(
            "nombre" => "fotográfia número 2",
            "longitud" => "55"
        ),
        "coo_n_ref_car" => Array(
            "nombre" => "coordenada norte",
            "longitud" => "15"
        ),
        "coo_e_ref_car" => Array(
            "nombre" => "coordenada este",
            "longitud" => "15"
        ),
        "sup_ter_ref_car" => Array(
            "nombre" => "sup terr",
            "longitud" => "55"
        ),
        "sup_inf_ref_car" => Array(
            "nombre" => "sup infraestructura",
            "longitud" => "55"
        ),
        "afe_sit_ref_car" => Array(
            "nombre" => "¿Existe alguna situación que afecte la producción de la empresa?"
        )
    );
    
    public function agregarReferenciasCartograficas($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , ReferenciasCartograficas::$validaciones);
        if($respuesta === true)
        {
            if($id_ref_car = ReferenciasCartograficas::create($datos))
            {
                return Herramientas::obtener_atributos($id_ref_car)->id_ref_car;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
