<?php
class Usuario extends ActiveRecord\Model
{
    //static $primary_key = 'id_usu';
    static $validaciones = Array(
        "nom_usu" => Array(
            "nombre" => "Nombre",
            "longitud" => "55",
            "NULL" => false
        ),
        "ape_usu" => Array(
            "nombre" => "Apellido",
            "longitud" => "55",
            "NULL" => false
        ),
        "log_usu" => Array(
            "nombre" => "Usuario",
            "longitud" => "15",
            "NULL" => false
        ),
        "pas_usu" => Array(
            "nombre" => "Contraseña",
            "longitud" => "55",
            "NULL" => false
        ),
        "ema_usu" => Array(
            "nombre" => "Email",
            "longitud" => "25",
            "NULL" => false
        ),
        "tel_usu" => Array(
            "nombre" => "Teléfono",
            "NULL" => false
        ),
        "dir_usu" => Array(
            "nombre" => "Dirección",
            "longitud" => "155",
            "NULL" => false
        )
    );
    
    static $has_many = array(
                            array('roles_usuarios'),
                            array('roles',
                                  'through' => 'roles_usuarios'
                                 )
                            );
    
    public function listarUsuarios()
    {                  
        $data = Usuario::consultarUsuario(Session::get("id_usu"));
        $campos = Array();        
        if($data->nom_rol == "transcriptor" || $data->nom_rol == "piloto")
        {
            $data = Array(
                $data = Usuario::find(Session::get("id_usu"))
                );
        }
        else if($data->nom_rol == "Jefe de Unidad")
        {
            $data = Array(
                $data = Usuario::find(Session::get("id_usu"))
                );
        }
        else if($data->nom_rol == "Estado Mayor")
        {
            $data = Array(
                $data = Usuario::find(Session::get("id_usu"))
                );
        }
        else
        {
            $data = Usuario::find('all');
        }
        //$data = Usuario::find('all');
        foreach ($data as $key => $value) 
        {
            array_push($campos, (object) $value->attributes());
            $roles = Array();
            $ids_roles = Array();
            foreach ($value->roles as $val) 
            {
                array_push($roles, $val->nom_rol);                
                array_push($ids_roles, $val->id);                
            }
            $campos[$key]->nom_rol = join(" , ", $roles);
            $campos[$key]->id_rol = join(" , ", $ids_roles);
        }
        return  (object)$campos;
    }
    
    public function consultarUsuario($id_usu)
    {        
        $usuario = Usuario::find($id_usu); 
        $atributos_usuario = $usuario->attributes();

        $roles = Array();
        $roles_id = Array();
        $pag_ini = Array();

        foreach ($usuario->roles as $rol) 
        {
            array_push($roles, $rol->nom_rol);
            array_push($roles_id, $rol->id);
        }
        $atributos_usuario['nom_rol'] = join(",", $roles);
        $atributos_usuario['id_rol'] = join(",", $roles_id);
        return (object)$atributos_usuario;
    }
    
    public function agregarUsuario($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Usuario::$validaciones);
        if($respuesta === true)
        {
            Usuario::connection()->transaction();
            if(!Usuario::find_by_log_usu(strtolower($datos["log_usu"])))
            {
                
                $role = $datos["id_rol"];                
                unset($datos["id_rol"]);
                $datos["log_usu"] = strtolower($datos["log_usu"]);
                $datos["pas_usu"] = md5($datos["pas_usu"]);                 
                
                if($id_usu = Usuario::create($datos)->id)
                {
                    $exito = TRUE;
                    foreach ($role as $value) 
                    {
                        $rol = Array(
                            "usuario_id" =>$id_usu,
                            "role_id"    =>$value,
                            "fec_usu_rol"=>"now()"
                            );
                        
                        if(!RolesUsuario::create($rol))
                        {
                            $exito = FALSE;
                        }
                    }

                    if($exito)
                    {
                        Usuario::connection()->commit();
                        return $id_usu;
                    }
                    else
                    {
                        Usuario::connection()->rollback();
                        return false;
                    }
                }
                else
                {
                    Usuario::connection()->rollback();
                    return 0;//Error
                }      
                
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarUsuario($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Planilla::$validaciones);
        if($respuesta === true)
        {
            Usuario::connection()->transaction();
            $usuario = Usuario::find($datos["id_usu"]);
            $roles = $datos["id_rol"];
            unset($datos["id_rol"]);
            $datos["id"] = $datos["id_usu"];
            unset($datos["id_usu"]);
            
            $exito = TRUE;
            if(!$usuario->update_attributes($datos))
            {                
                $exito = FALSE;
            }
            else
            {
                if(
                    RolesUsuario::count(Array("conditions"=>"usuario_id = ".$datos['id'])) > 0 &&
                    !RolesUsuario::delete_all(Array("conditions"=>"usuario_id = ".$datos['id'])))
                {
                    $exito = FALSE;
                }
                else
                {
                    foreach ($roles as $value) 
                    {
                        $rol = Array(
                            "usuario_id" =>$datos["id"],
                            "role_id" =>$value,
                            "fec_usu_rol"=>"now()"
                            );                
                        if(!RolesUsuario::create($rol))
                        {
                            $exito = FALSE;
                        }
                    }
                }
                
                if($exito)
                {
                    Usuario::connection()->commit();
                    return true;
                }
                else 
                {
                    Usuario::connection()->rollback();
                    return false;
                }
            }
        }
        else 
        {
            return $respuesta;
        }
        
        
        
        return TRUE;
    }
    
    public function eliminarUsuario($id_usu)
    {             
        try 
        {
            Usuario::connection()->transaction();
            $usuario = Usuario::find($id_usu);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            Usuario::connection()->rollback();
            return FALSE;
            exit();
        }
        
        $exito = TRUE;
        if(
            RolesUsuario::count(Array("conditions"=>"usuario_id = $id_usu")) > 0 &&
            !RolesUsuario::delete_all(Array("conditions"=>"usuario_id = $id_usu")))
        {
            $exito = FALSE;
        }
        
        
        if(
            PermisosUsuario::count(Array("conditions"=>"id_usu = $id_usu")) > 0 &&
            !PermisosUsuario::delete_all(Array("conditions"=>"id_usu = $id_usu")))
        {
            $exito = FALSE;
        }
        
        if(!$usuario->delete())
        {
            $exito = FALSE;
        }
        
        if($exito)
        {
            Usuario::connection()->commit();
            return TRUE;
        }
        else 
        {
            Usuario::connection()->rollback();
            return FALSE;
        }
        
    }          
}
?>
