<?php
class CodigoCatastral extends ActiveRecord\Model
{
    static $table_name = 'codigo_catastral';    
    static $primary_key = 'id_cod_cat';    
    static $validaciones = Array(
        "efed_cod_cat" => Array(
            "nombre" => "Entidad Federal",
            "longitud" => "20",
            "NULL" => false
        ),
        "mun_cod_cat" => Array(
            "nombre" => "Municipio",
            "longitud" => "20",
            "NULL" => false
        ),
        "parr_cod_cat" => Array(
            "nombre" => "Parroquia",
            "longitud" => "20",
            "NULL" => false
        ),
        "amb_cod_cat" => Array(
            "nombre" => "Ámbito",
            "longitud" => "20",
            "NULL" => false
        ),
        "sec_cod_cat" => Array(
            "nombre" => "Sector",
            "longitud" => "20",
            "NULL" => false
        ),
        "man_cod_cat" => Array(
            "nombre" => "Manzana",
            "longitud" => "20",
            "NULL" => false
        ),
        "par_cod_cat" => Array(
            "nombre" => "Parcela",
            "longitud" => "20",
            "NULL" => false
        ),
        "sbp_cod_cat" => Array(
            "nombre" => "Sub-Parcela",
            "longitud" => "20",
            "NULL" => false
        ),
        "niv_cod_cat" => Array(
            "nombre" => "Nivel",
            "longitud" => "20",
            "NULL" => false
        ),
        "und_cod_cat" => Array(
            "nombre" => "Unidad Inmobiliaria",
            "longitud" => "20",
            "NULL" => false
        )
    );
    
    public function listarCodigoCatastrals()
    {          
        $data = CodigoCatastral::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarCodigoCatastral($id)
    {       
        $tip_arm = CodigoCatastral::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarCodigoCatastral($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , CodigoCatastral::$validaciones);
        if($respuesta === true)
        {            
            if(!CodigoCatastral::find_by_efed_cod_cat_and_mun_cod_cat_and_parr_cod_cat_and_sec_cod_cat_and_man_cod_cat_and_par_cod_cat_and_sbp_cod_cat_and_und_cod_cat($datos["efed_cod_cat"],$datos["mun_cod_cat"],$datos["parr_cod_cat"],$datos["sec_cod_cat"],$datos["man_cod_cat"],$datos["par_cod_cat"],$datos["sbp_cod_cat"],$datos["und_cod_cat"]))
            {
                if($id_cod_cat = CodigoCatastral::create($datos))
                {
                    return Herramientas::obtener_atributos($id_cod_cat)->id_cod_cat;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarCodigoCatastral($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , CodigoCatastral::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = CodigoCatastral::find($datos["id_cod_cat"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarCodigoCatastral($id)
    {             
        try 
        {
            $tip_arm = CodigoCatastral::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
