<?php
class PermisosUsuario extends ActiveRecord\Model
{
    /*static $belongs_to = array(
                            array('usuario'),
                            array('Permiso')
       );*/
    
    public function consultarUsuarioPermisos($id_usu , $id_per)
    {
        $campo = Array(
                        "id_per" => $id_per , 
                        "id_usu" => $id_usu
                        );
        
        return $this->_db->consultar($tabla, $campo);        
    }
    
    public function consultarUsuarioPermisosTodos($id_usu , $id_rol)
    {        
        if(strpos($id_rol , ",") > 0)
        {
            $tmp = explode(",", $id_rol);
            $id_rol = join(" OR r.id = ", $tmp);
        }
        
        $sql = "SELECT DISTINCT ON (p.id_per)                    
                    p.id_per,
                    fk_per, 
                    coalesce((SELECT val_per_rol FROM permisos_roles WHERE id_per = p.id_per AND ( id_rol = $id_rol ) LIMIT 1)::integer , 0) AS estado_rol,
                    coalesce((SELECT val_per_usu FROM permisos_usuarios WHERE id_per = p.id_per AND id_usu = $id_usu LIMIT 1)::integer , 0) AS estado_usu
                FROM 
                    permisos p,
                    roles r
                WHERE 
                    nom_per <> 'index'
                AND
                    ( r.id = $id_rol )
                ORDER BY 
                    p.id_per, 
                    fk_per, 
                    nom_rol";
        
        
        $permiso = Array();
        foreach (PermisosUsuario::find_by_sql($sql) as $perm) 
        {
            array_push($permiso, (object)$perm->attributes());
        }
        
        return $permiso;
    }
    
    public function agregarUsuarioPermiso($id_usu , $id_per)
    {        
        if(!PermisosUsuario::find_by_id_usu_and_id_per($id_usu,$id_per))
        {
            $dato = Array(
                            "id_usu" => $id_usu, 
                            "id_per" => $id_per, 
                            "fec_per_usu" => "now()", 
                            "val_per_usu" => "1"
                         );            
            $resultado = PermisosUsuario::create($dato);
            if($resultado)
            {        
                return TRUE;             
            }
            else
            {
                return FALSE; 
            }
        }
        else
        {
            return true;
        }        
    }
   
    public function eliminarUsuarioPermiso($id_usu , $id_per)
    {
        if($permiso = PermisosUsuario::find_by_id_usu_and_id_per($id_usu,$id_per))
        {
            if($permiso->delete())        
            {                        
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }  
    
    public function agregarIndex($id_usu)
    {
        $sql = "SELECT * FROM permisos WHERE nom_per = 'index'";                
        $datos = $this->_db->consultar($sql);;
        
        if($datos)        
        {                        
            foreach ($datos as $value) 
            {                
                $campos = Array(
                            "id_usu" => $id_usu, 
                            "id_per" => $value->id_per, 
                            "fec_per_usu" => "now()", 
                            "val_per_usu" => "1"
                         );        
                
                $resultado = $this->_db->agregar("permisos_usuarios", $campos, "id_per_usu");
            }
        }
        else
        {
            Die("Error al agregar los index al usuario.");
        }        
    }
}
?>
