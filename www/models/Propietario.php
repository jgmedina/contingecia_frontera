<?php
class Propietario extends ActiveRecord\Model
{
    static $table_name = 'propietario';    
    static $primary_key = 'id_pro';    
    static $validaciones = Array(
        "tip_pro" => Array(
            "nombre" => "Tipo de propietario",
            "longitud" => "10",
            "NULL" => false
        ),
        "raz_nom_pro" => Array(
            "nombre" => "Nombre o Razón Social",
            "longitud" => "155",
            "NULL" => false
        ),
        "tip_doc_pro" => Array(
            "nombre" => "Tipo de documento de identidad",
            "longitud" => "1",
            "NULL" => false
        ),
        "doc_ide_pro" => Array(
            "nombre" => "Número de documento de identidad",
            "longitud" => "10",
            "NULL" => false
        )
    );
    
    public function listarPropietarios()
    {          
        $data = Propietario::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarPropietario($id)
    {       
        $tip_arm = Propietario::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarPropietario($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Propietario::$validaciones);
        if($respuesta === true)
        {
            if(!Propietario::find_by_doc_ide_pro($datos["doc_ide_pro"]))
            {
                if($id_pro = Propietario::create($datos))
                {
                    return Herramientas::obtener_atributos($id_pro)->id_pro;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarPropietario($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Propietario::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = Propietario::find($datos["id_pro"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarPropietario($id)
    {             
        try 
        {
            $tip_arm = Propietario::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
