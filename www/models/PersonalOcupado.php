<?php
class PersonalOcupado extends ActiveRecord\Model
{
    static $table_name = 'personal_ocupado';    
    static $primary_key = 'id_per_ocu';    
    static $validaciones = Array(
        "tot_hom_per_ocu" => Array(
            "nombre" => "total de hombres",
            "null" => false
        ),
        "tot_muj_per_ocu" => Array(
            "nombre" =>  "total de mujeres",
            "null" => false
        ),
        "tot_ven" => Array(
            "nombre" =>  "total de venezolanos"
        ),
        "tot_col_per_ocu" => Array(
            "nombre" =>  "total de colombianos"
        ),
        "tot_otr_per_ocu" => Array(
            "nombre" =>  "total de otros paises"
        ),
        "tot_ger_per_ocu" => Array(
            "nombre" =>  "total de gerencial"
        ),
        "tot_adm_per_ocu" => Array(
            "nombre" =>  "total de administración"
        ),
        "tot_pro_per_ocu" => Array(
            "nombre" =>  "total de producción"
        ),
        "tot_ven_per_ocu" => Array(
            "nombre" =>  "total de ventas"
        ),
        "tot_com_per_ocu" => Array(
            "nombre" =>  "total de comercialización"
        ),
        "cap_max_per_ocu" => Array(
            "nombre" =>  "Capacidad máxima"
        )
    );
    
    public function agregarPersonalOcupado($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , PersonalOcupado::$validaciones);
        if($respuesta === true)
        {
            if($id_per_ocu = PersonalOcupado::create($datos))
            {
                return Herramientas::obtener_atributos($id_per_ocu)->id_per_ocu;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
