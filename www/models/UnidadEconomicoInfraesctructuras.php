<?php
class UnidadEconomicoInfraesctructuras extends ActiveRecord\Model
{
    static $table_name = 'unidad_economico_infraesctructuras';    
    static $primary_key = 'id_uni_eco_inf';    
    static $validaciones = Array(
        "id_inf" => Array(
            "nombre" => "Identificador de la tabla infraestructura",
            "NULL" => false
        ),
        "id_uni_eco" => Array(
            "nombre" => "Identificador de la tabla unidad economica",
            "NULL" => false
        ),
        "can_uni_eco_inf" => Array(
            "nombre" => "Cantidad",
            "NULL" => false
        ),
        "est_uni_eco_inf" => Array(
            "nombre" => "Estado de la infraestructura",
            "longitud" => "1",
            "NULL" => false
        )
    );
    
    public function agregarUnidadEconomicoInfraesctructuras($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , UnidadEconomicoInfraesctructuras::$validaciones);
        if($respuesta === true)
        {
            if($id_uni_eco_inf = UnidadEconomicoInfraesctructuras::create($datos))
            {
                return Herramientas::obtener_atributos($id_uni_eco_inf)->id_uni_eco_inf;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
