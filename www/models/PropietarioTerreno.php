<?php
class PropietarioTerreno extends ActiveRecord\Model
{
    static $table_name = 'propietario_terreno';    
    static $primary_key = 'id_pro_ter';    
    static $validaciones = Array(
        "tip_pro_ter" => Array(
            "nombre" => "Tipo de propietario del terreno",
            "longitud" => "10",
            "NULL" => false
        ),
        "raz_nom_pro_ter" => Array(
            "nombre" => "Nombre o Razón Social",
            "longitud" => "155",
            "NULL" => false
        ),
        "tip_doc_pro_ter" => Array(
            "nombre" => "Tipo de documento de identidad",
            "longitud" => "1",
            "NULL" => false
        ),
        "doc_ide_pro_ter" => Array(
            "nombre" => "Número de documento de identidad",
            "longitud" => "10",
            "NULL" => false
        )
    );
    
    public function agregarPropietarioTerreno($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , PropietarioTerreno::$validaciones);
        if($respuesta === true)
        {
            if(!PropietarioTerreno::find_by_doc_ide_pro_ter($datos["doc_ide_pro_ter"]))
            {
                if($id_pro_ter = PropietarioTerreno::create($datos))
                {
                    return Herramientas::obtener_atributos($id_pro_ter)->id_pro_ter;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
