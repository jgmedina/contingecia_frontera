<?php
class DatosProductivos extends ActiveRecord\Model
{
    static $table_name = 'datos_productivos';    
    static $primary_key = 'id_dat_pro';    
    static $validaciones = Array(
        "cap_max_dat_pro" => Array(
            "nombre" => "Capacidad Máxima",
            "longitud" => "15"
        ),
        "und_cap_max_dat_pro" => Array(
            "nombre" => "Unidad de medida de la capacidad máxima",
            "longitud" => "55"
        ),
        "cap_exp_dat_pro" => Array(
            "nombre" => "¿posee capacidad de exportación?"
        ),
        "und_cap_exp_dat_pro" => Array(
            "nombre" => "Unidad de medida de la capacidad de exportación",
            "longitud" => "55"
        ),
        "cap_act_dat_pro" => Array(
            "nombre" => "Capacidad Actual"
        ),
        "und_cap_act_dat_pro" => Array(
            "nombre" => "unidad de medida de la capacidad actual",
            "longitud" => "55"
        ),
        "ope_dat_pro" => Array(
            "nombre" => "Operatividad"
        ),
        "id_mat_pri" => Array(
            "nombre" => "identificador de la tabla materia_primas",
            "longitud" => "55"
        ),
        "id_des_pro" => Array(
            "nombre" => "identificador de la tabla destino_producto",
            "longitud" => "55"
        ),
        "id_proo_pri" => Array(
            "nombre" => "identificador de la tabla proovedores_principales",
            "longitud" => "55"
        ),
        "maq_dat_pro" => Array(
            "nombre" => "¿Posee maquinaria?"
        ),
        "cua_dat_pro" => Array(
            "nombre" => "¿Cuales?",
            "longitud" => "155"
        ),
        "val_anu_ven_dat_pro" => Array(
            "nombre" => "valor anual de venta"
        ),
        "val_anu_com_dat_pro" => Array(
            "nombre" => "valor anual de compra"
        ),
        "mon_ven_dato_pro" => Array(
            "nombre" => "Tipo de Moneda de venta anual",
            "longitud" => "5"
        ),
        "mon_com_dato_pro" => Array(
            "nombre" => "Tipo de Moneda de compra anual",
            "longitud" => "5"
        )   
    );
    
    public function agregarDatosProductivos($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , DatosProductivos::$validaciones);        
        if($respuesta === true)
        {
            if($id_dat_pro = DatosProductivos::create($datos))
            {
                return Herramientas::obtener_atributos($id_dat_pro)->id_dat_pro;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
