<?php
class PermisosRole extends ActiveRecord\Model
{
    public function agregarRolPermiso($id_rol , $id_per)
    {
        $permiso = Array(                    
                    'id_rol'=> $id_rol, 
                    'id_per'=> $id_per,
                    "fec_per_rol" => "now()",
                    "val_per_rol" => "1"
                    );        
        
        try 
        {
            if(PermisosRole::create($permiso))
            {
                return PermisosRole::find_by_id_rol_and_id_per($id_rol,$id_per)->id;
            }
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {            
            return FALSE; 
            exit();
        }
    }
    
    public function eliminarRolPermiso($id_rol , $id_per)
    {
        $permiso = PermisosRole::find_by_id_rol_and_id_per($id_rol,$id_per);            
        $permiso->delete();
        return TRUE;
    }          
}
?>
