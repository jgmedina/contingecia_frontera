<?php
class SituacionesPresentadas extends ActiveRecord\Model
{
    static $table_name = 'situaciones_presentadas';    
    static $primary_key = 'id_sit_pre';    
    static $validaciones = Array(
        "res_sit_pre" => Array(
            "nombre" => "respuesta de la unidad",
            "longitud" => "15"
        ),
        "cod_res_sit_pre" => Array(
            "nombre" => "código de la respuesta de la unidad",
            "longitud" => "15"
        ),
        "inf_uni_sit_pre" => Array(
            "nombre" => "información suministrada poco clara o  con respecto adudosa",
            "longitud" => "15"
        ),
        "cod_inf_sit_pre" => Array(
            "nombre" => "código de la información suministrada poco clara o  con respecto adudosa",
            "longitud" => "15"
        ),
        "obs_sit_pre" => Array(
            "nombre" => "observaciones directa dentro de la unidad",
            "longitud" => "30"
        ),
        "cod_obs_sit_pre" => Array(
            "nombre" => "código de la observaciones directa dentro de la unidad",
            "longitud" => "15"
        ),
        "otr_obs_sit_pre" => Array(
            "nombre" => "Otras Novedades Observadas",
            "longitud" => "15"
        )
    );
    
    public function agregarSituacionesPresentadas($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , SituacionesPresentadas::$validaciones);
        if($respuesta === true)
        {
            if($id_sit_pre = SituacionesPresentadas::create($datos))
            {
                return Herramientas::obtener_atributos($id_sit_pre)->id_sit_pre;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
