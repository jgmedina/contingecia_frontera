<?php
class Empadronador extends ActiveRecord\Model
{
    static $table_name = 'empadronadores';    
    static $primary_key = 'id_empa';    
    static $validaciones = Array(
        "nom_empa" => Array(
            "nombre" => "nombre del empadronador",
            "longitud" => "155",
            "NULL" => false
        ),
        "tip_doc_empa" => Array(
            "nombre" => "tipo de de documento del empadronador",
            "longitud" => "1",
            "NULL" => false
        ),
        "doc_ide_empa" => Array(
            "nombre" => "Número de documento de identidad",
            "longitud" => "10",
            "NULL" => false
        ),
        "tel_empa" => Array(
            "nombre" => "teléfono del empadronador",
            "longitud" => "12",
            "NULL" => false
        ),
        "ema_empa" => Array(
            "nombre" => "Correo del empadronador",
            "longitud" => "55",
            "NULL" => false
        ),
        "rel_emp_empa" => Array(
            "nombre" => "relación con la empresa",
            "longitud" => "55"
        )
    );
    
    public function agregarEmpadronador($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Empadronador::$validaciones);
        if($respuesta === true)
        {
            if(!Empadronador::find_by_doc_ide_empa($datos["doc_ide_empa"]))
            {
                if($id_empa = Empadronador::create($datos))
                {
                    return Herramientas::obtener_atributos($id_empa)->id_empa;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
