<?php
class UnidadEconomica extends ActiveRecord\Model
{
    static $table_name = 'unidad_economica';    
    static $primary_key = 'id_uni_eco';    
    static $validaciones = Array(
        "id_ubi" => Array(
            "nombre" => "Identificador del ubigeo",
            "longitud" => "55",
            "NULL" => false
        ),
        "id_ide" => Array(
            "nombre" => "Identificador de la Sección I",
            //"longitud" => "",
            "NULL" => false
        ),
        "id_emp" => Array(
            "nombre" => "Identificador de la Empresa",
            "longitud" => "27",
            "NULL" => false
        ),
        "id_pro" => Array(
            "nombre" => "Identificador del propietario",
            "longitud" => "7"
        ),
        "id_pro_ter" => Array(
            "nombre" => "Identificador del propietario del terreno",
            "longitud" => "7"
        ),
        "id_pro_inf" => Array(
            "nombre" => "Identificador del propietario de la infraestructura",
            "longitud" => "7"
        ),
        "id_niv_act" => Array(
            "nombre" => "Identificador de la Sección III",
            "longitud" => "8"
        ),
        "id_dat_pro" => Array(
            "nombre" => "Identificador de la Sección V",
            "longitud" => "255"
        ),
        "id_per_ocu" => Array(
            "nombre" => "Identificador de la Sección VI"
        ),
        "id_ref_car" => Array(
            "nombre" => "Identificador de la Sección VII"
        ),
        "id_rec_pro" => Array(
            "nombre" => "Identificador de la Sección VIII"
        ),
        "id_ent" => Array(
            "nombre" => "Identificador del entrevistado"
        ),
        "id_empa" => Array(
            "nombre" => "Identificador del empadronador"
        ),
        "id_sit_pre" => Array(
            "nombre" => "Identificador de la Sección IX"
        ),
        "obs_uni_eco" => Array(
            "nombre" => "Observación",
            "longitud" => "55",
        )/*,
        "rut_arc_uni_eco" => Array(
            "nombre" => "Ruta del Shafile",
            //"longitud" => "55",
        )*/
    );
    
    public function agregarUnidadEconomica($datos)
    {      
        $exito = TRUE;
        $errores = Array();
        UnidadEconomica::connection()->transaction();
        if($id_ubi = Ubigeo::agregarUbigeo($datos["ubigeo"]))
        {
            if(is_array($id_ubi))
            {
                $errores = array_merge($errores,$id_ubi);
                $exito = FALSE;
            }
        }

        /*
        if($id_cod_cat = CodigoCatastral::agregarCodigoCatastral($datos["cod_cat"]))
        {
            if(is_array($id_cod_cat))
            {
                $errores = array_merge($errores,$id_cod_cat);
                $exito = FALSE;
            }
        }
        */
        if($exito)
        {
            $datos["identificacion"]["id_ubi"] = $id_ubi;
            //$datos["identificacion"]["id_cod_cat"] = $id_cod_cat;
            if($id_ide = Identificacion::agregarIdentificacion($datos["identificacion"]))
            {
                if(is_array($id_ide))
                {
                    $errores = array_merge($errores,$id_ide);
                    $exito = FALSE;
                }
            }
        }
        else
        {
            return $errores;
        }
        
        if($id_pro = Propietario::agregarPropietario($datos["propietario"]))
        {
            if(is_array($id_pro))
            {
                $errores = array_merge($errores,$id_pro);
                $exito = FALSE;
            }
        }

        if($id_pro_ter = PropietarioTerreno::agregarPropietarioTerreno($datos["propietario_ter"]))
        {
            if(is_array($id_pro_ter))
            {
                $errores = array_merge($errores,$id_pro_ter);
                $exito = FALSE;
            }
        }

        if($id_pro_inf = PropietarioInfraestructura::agregarPropietarioInfraestructura($datos["propietario_inf"]))
        {
            if(is_array($id_pro_inf))
            {
                $errores = array_merge($errores,$id_pro_inf);
                $exito = FALSE;
            }
        }
        
        if($exito)
        {
            $datos["empresas"]["id_pro"] = $id_pro;
            $datos["empresas"]["id_pro_ter"] = $id_pro_ter;
            $datos["empresas"]["id_pro_inf"] = $id_pro_inf;
            if($id_emp = Empresa::agregarEmpresa($datos["empresas"]))
            {
                if(is_array($id_emp))
                {
                    $errores = array_merge($errores,$id_emp);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            if($id_niv_act = NivelActividad::agregarNivelActividad($datos["nivel_act"]))
            {
                if(is_array($id_niv_act))
                {
                    $errores = array_merge($errores,$id_niv_act);
                    $exito = FALSE;
                }
            }
        }
        
        if($id_mat_pri = MateriasPrimas::agregarMateriasPrimas($datos["materias"]))
        {
            if(is_array($id_mat_pri))
            {
                $errores = array_merge($errores,$id_mat_pri);
                $exito = FALSE;
            }
        }
        
        if($id_des_pro = DestinoProduccion::agregarDestinoProduccion($datos["destino"]))
        {
            if(is_array($id_des_pro))
            {
                $errores = array_merge($errores,$id_des_pro);
                $exito = FALSE;
            }
        }
        
        if($id_proo_pri = ProveedoresPrincipales::agregarProveedoresPrincipales($datos["proveedores"]))
        {
            if(is_array($id_proo_pri))
            {
                $errores = array_merge($errores,$id_proo_pri);
                $exito = FALSE;
            }
        }
        
        if($exito)
        {
            $datos["datos_productivos"]["id_mat_pri"] = $id_mat_pri;
            $datos["datos_productivos"]["id_des_pro"] = $id_des_pro;
            $datos["datos_productivos"]["id_proo_pri"] = $id_proo_pri;
            if($id_dat_pro = DatosProductivos::agregarDatosProductivos($datos["datos_productivos"]))
            {
                if(is_array($id_dat_pro))
                {
                    $errores = array_merge($errores,$id_dat_pro);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            if($id_per_ocu = PersonalOcupado::agregarPersonalOcupado($datos["personal_ocu"]))
            {
                if(is_array($id_per_ocu))
                {
                    $errores = array_merge($errores,$id_per_ocu);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            if($id_ref_car = ReferenciasCartograficas::agregarReferenciasCartograficas($datos["referencias_car"]))
            {
                if(is_array($id_ref_car))
                {
                    $errores = array_merge($errores,$id_ref_car);
                    $exito = FALSE;
                }
            }
        }
        /*
        Herramientas::debug($dato["referencias_car"]["afe_sit_ref_car"]);
        Herramientas::debug($dato["referencias_car"]["afe_sit_ref_car"] == "TRUE");
        Herramientas::debug($dato["referencias_car"]["afe_sit_ref_car"] == TRUE);
        die();
        */
        if($dato["referencias_car"]["afe_sit_ref_car"] == "TRUE" || $dato["referencias_car"]["afe_sit_ref_car"] == TRUE)
        {
            if($exito)
            {
                if($id_ref_car = RecuperacionProduccion::agregarRecuperacionProduccion($datos["referencias_car"]))
                {
                    if(is_array($id_ref_car))
                    {
                        $errores = array_merge($errores,$id_ref_car);
                        $exito = FALSE;
                    }
                }
            }
        }
        
        if($exito)
        {
            if($id_ent = Entrevistado::agregarEntrevistado($datos["entrevistado"]))
            {
                if(is_array($id_ent))
                {
                    $errores = array_merge($errores,$id_ent);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            if($id_empa = Empadronador::agregarEmpadronador($datos["empadronador"]))
            {
                if(is_array($id_empa))
                {
                    $errores = array_merge($errores,$id_empa);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            if($id_sit_pre = SituacionesPresentadas::agregarSituacionesPresentadas($datos["situaciones_pre"]))
            {
                if(is_array($id_sit_pre))
                {
                    $errores = array_merge($errores,$id_sit_pre);
                    $exito = FALSE;
                }
            }
        }
        
        if($exito)
        {
            $datos["unidad_eco"]["id_ide"] = $id_ide;
            $datos["unidad_eco"]["id_emp"] = $id_emp;
            $datos["unidad_eco"]["id_pro"] = $id_pro;
            $datos["unidad_eco"]["id_pro_ter"] = $id_pro_ter;
            $datos["unidad_eco"]["id_pro_inf"] = $id_pro_inf;
            $datos["unidad_eco"]["id_niv_act"] = $id_niv_act;
            $datos["unidad_eco"]["id_dat_pro"] = $id_dat_pro;
            $datos["unidad_eco"]["id_per_ocu"] = $id_per_ocu;
            $datos["unidad_eco"]["id_ref_car"] = $id_ref_car;
            $datos["unidad_eco"]["id_rec_pro"] = $id_rec_pro;
            $datos["unidad_eco"]["id_ent"] = $id_ent;
            $datos["unidad_eco"]["id_empa"] = $id_empa;
            $datos["unidad_eco"]["id_sit_pre"] = $id_sit_pre;
            
            $respuesta = Herramientas::validaciones($datos["unidad_eco"] , UnidadEconomica::$validaciones);
            if($respuesta === true)
            {
                if($id_uni_eco = UnidadEconomica::create($datos["unidad_eco"])->id_uni_eco)
                {
                    $datos["uni_eco_inf"] = Herramientas::transformarDatosValidos($datos["uni_eco_inf"]);
                    foreach ($datos["uni_eco_inf"] as $value) 
                    {
                        $value["id_uni_eco"] = $id_uni_eco;
                        if($id_uni_eco_inf = UnidadEconomicoInfraesctructuras::agregarunidadEconomicoInfraesctructuras($value))
                        {
                            if(is_array($id_uni_eco_inf))
                            {
                                $errores = array_merge($errores,$id_uni_eco_inf);
                                $exito = FALSE;
                            }
                        }
                    }

                    $datos["sector_economico"] = Herramientas::transformarDatosValidos($datos["sector_economico"]);
                    
                    foreach ($datos["sector_economico"] as $value) 
                    {
                        $value["id_uni_eco"] = $id_uni_eco;
                        if($id_sec_eco = SectorEconomico::agregarSectorEconomico($value))
                        {
                            if(is_array($id_sec_eco))
                            {
                                $errores = array_merge($errores,$id_sec_eco);
                                $exito = FALSE;
                            }
                        }
                    }

                    $_FILES["fotografias"] = Herramientas::transformarDatosValidos($_FILES["fotografias"]);
                    $ok = TRUE;
                    foreach ($_FILES["fotografias"] as $key => $value) 
                    {
                        if(!empty($value["name"]))
                        {
                            
                            if(!in_array($value['type'], Array("image/jpeg","image/jpg","image/png")))
                            {
                                $errores = array_merge($errores,"El formato de la foto del personal es incorrecto.");
                                $ok = FALSE;
                            }

                            if ($value['name'] != "") 
                            {
                                $archivo = $value['name'];
                                $tmp = explode(".", $archivo);
                                $ext = $tmp[1];
                                $archivo = md5($id_uni_eco)."_$key.$ext";
                                $destino = "./public/img/fotografias/$archivo";
                                
                                if (!@copy($value['tmp_name'],$destino))                         
                                {
                                    
                                    $errors= error_get_last();
                                    $errores = array_merge($errores,"Error al subir el archivo<br/>\n".$errors['message']);
                                }
                                else
                                {
                                    $referencia_car = ReferenciasCartograficas::find($id_ref_car);
                                    $campo = ($key == 0)?"fot_1_ref_car":"fot_2_ref_car";
                                    if(!$referencia_car->update_attributes(Array($campo=>$archivo)))
                                    {
                                        $exito = FALSE;
                                    }
                                }
                            } 
                        }
                    }
                }
                else
                {
                    $errores = array_merge($errores,$id_uni_eco);
                    return $errores;
                }

                if($exito)
                {
                    UnidadEconomica::connection()->commit();
                    return $exito;
                }
                else
                {
                    UnidadEconomica::connection()->rollback();
                    return $errores;
                }
            }
            else
            {
                $errores = array_merge($errores,$respuesta);
                return $errores;
            }
            
        }
		else
        {
            return $errores;
        }
    }
    
    public function listarEntidades($ine = FALSE)
    {      
        if($ine)
        {
            $sql = "SELECT * FROM entidad_ine ORDER BY nom_ent";
        }
        else
        {
            $sql = "SELECT * FROM estados ORDER BY nom_est";
        }
        return (array)  Herramientas::obtener_atributos(UnidadEconomica::find_by_sql($sql));
    }
    public function listarMunicipios($id_ent , $ine = FALSE)
    {      
        if((is_string($ine) && strtolower($ine) != "false"))
        {
            $sql = "SELECT * FROM municipios_ine WHERE cod_ent = $id_ent ORDER BY  descripcion";
        }
        else
        {
            $sql = "SELECT * FROM municipios WHERE cod_efed = $id_ent ORDER BY nom_mun";
        }
        return (array)  Herramientas::obtener_atributos(UnidadEconomica::find_by_sql($sql));
    }
    public function listarParroquias($cod_ent,$id_mun , $ine = FALSE)
    {       
        if((is_string($ine) && strtolower($ine) != "false"))
        {
            $sql = "SELECT * FROM  parroquias_ine WHERE cod_ent = $cod_ent AND cod_mun  = $id_mun ORDER BY descripcion";
        }
        else
        {
            $sql = "SELECT * FROM parroquias WHERE cod_efed = $cod_ent AND cod_mun  = $id_mun ORDER BY  nom_par";
        }
        return (array)  Herramientas::obtener_atributos(UnidadEconomica::find_by_sql($sql));
    }
    
    public function listarCentroPoblado($cod_ent , $cod_mun , $cod_par)
    {      
        $sql = "SELECT  * FROM centros_poblados_ine WHERE cod_ent = $cod_ent AND cod_mun = $cod_mun AND cod_parrq = $cod_par ORDER BY  descripcion";
        return (array)  Herramientas::obtener_atributos(UnidadEconomica::find_by_sql($sql));
    }
}
?>
