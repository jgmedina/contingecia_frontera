<?php
class Entrevistado extends ActiveRecord\Model
{
    static $table_name = 'entrevistados';    
    static $primary_key = 'id_ent';    
    static $validaciones = Array(
        "nom_ent" => Array(
            "nombre" => "nombre del entrevistados",
            "longitud" => "155",
            "NULL" => false
        ),
        "tip_doc_ent" => Array(
            "nombre" => "tipo de de documento del entrevistados",
            "longitud" => "1",
            "NULL" => false
        ),
        "doc_ide_ent" => Array(
            "nombre" => "Número de documento de identidad",
            "longitud" => "10",
            "NULL" => false
        ),
        "tel_ent" => Array(
            "nombre" => "teléfono del entrevistados",
            "longitud" => "12",
            "NULL" => false
        ),
        "ema_ent" => Array(
            "nombre" => "Correo del entrevistados",
            "longitud" => "55",
            "NULL" => false
        ),
        "rel_emp_ent" => Array(
            "nombre" => "relación con la empresa",
            "longitud" => "55"
        )
    );
    
    public function agregarEntrevistado($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Entrevistado::$validaciones);
        if($respuesta === true)
        {
            if(!Entrevistado::find_by_doc_ide_ent($datos["doc_ide_ent"]))
            {
                if($id_ent = Entrevistado::create($datos))
                {
                    return Herramientas::obtener_atributos($id_ent)->id_ent;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
