<?php
class DestinoProduccion extends ActiveRecord\Model
{
    static $table_name = 'destino_producciones';    
    static $primary_key = 'id_des_pro';    
    static $validaciones = Array(
        "des_des_pro" => Array(
            "nombre" => "descripción de destino de producción",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function agregarDestinoProduccion($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , DestinoProduccion::$validaciones);
        if($respuesta === true)
        {
            if($id_des_pro = DestinoProduccion::create($datos))
            {
                return Herramientas::obtener_atributos($id_des_pro)->id_des_pro;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
}
?>
