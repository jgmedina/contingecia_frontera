<?php
class Persona extends ActiveRecord\Model
{
    static $table_name = 'personas';    
    static $primary_key = 'id_per';    
    static $validaciones = Array(
        "nom_per" => Array(
            "nombre" => "Nombre",
            "longitud" => "155",
            "NULL" => false
        ),
        "tel_per" => Array(
            "nombre" => "Teléfono",
            "longitud" => "12",
            "NULL" => false
        ),
        "ema_per" => Array(
            "nombre" => "E-Mail",
            "longitud" => "55",
            "NULL" => false
        )
    );
    
    public function listarPersonas()
    {          
        $data = Persona::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarPersona($id)
    {       
        $tip_arm = Persona::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarPersona($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Persona::$validaciones);        
        if($respuesta === true)
        {
            if(!Persona::find_by_ema_per($datos["ema_per"]))
            {
                
                $datos["tel_per"] = str_replace("-", "", $datos["tel_per"]);
                if($id_per = Persona::create($datos))
                {
                    return Herramientas::obtener_atributos($id_per)->id_per;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarPersona($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Persona::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = Persona::find($datos["id_per"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarPersona($id)
    {             
        try 
        {
            $tip_arm = Persona::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
