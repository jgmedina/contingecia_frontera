<?php
class ValidadoresCampo extends ActiveRecord\Model
{
    static $connection = "app";
    static $table_name = 'validadores_campos';    
    //static $primary_key = 'id_val';    
    
    public function agregarValidacionesCampos($atributos)
    {
        return ValidadoresCampo::create($atributos);
    }            
    
    public function eliminarValidacionesCampo($campo , $controlador , $metodo)
    {
        $id_campo = OrigenCampo::find_by_nombre_campo_and_controlador_and_metodo($campo , $controlador , $metodo)->id;
        $val_cam = ValidadoresCampo::find_all_by_id_campo($id_campo);
        if(count($val_cam) > 0)
        {
            ValidadoresCampo::connection()->transaction();
            $exito = TRUE;
            foreach ($val_cam as $value) 
            {
                if(!$value->delete())
                {
                    $exito = FALSE;
                }
            }
            
            if($exito)
            {
                ValidadoresCampo::connection()->commit();
            }
            else
            {
                ValidadoresCampo::connection()->rollback();
            }
        }
    }            
}
?>
