<?php
class Ubigeo extends ActiveRecord\Model
{
    static $table_name = 'ubigeo';    
    static $primary_key = 'id_ubi';    
    static $validaciones = Array(
        "ent_fed_ubi" => Array(
            "nombre" => "Entidad Federal",
            "longitud" => "5",
            "NULL" => false
        ),
        "mun_ubi" => Array(
            "nombre" => "Municipio",
            "longitud" => "5",
            "NULL" => false
        ),
        "parr_ubi" => Array(
            "nombre" => "Parroquia",
            "longitud" => "5",
            "NULL" => false
        ),
        "seg_sec_ubi" => Array(
            "nombre" => "Segmento/Sector",
            "longitud" => "5",
            "NULL" => false
        ),
        "cen_pob_ubi" => Array(
            "nombre" => "Centro Poblado",
            "longitud" => "5",
            "NULL" => false
        ),
        "man_ubi" => Array(
            "nombre" => "Manzana",
            "longitud" => "5",
            "NULL" => false
        ),
        "parc_ubi" => Array(
            "nombre" => "Parcela",
            "longitud" => "5",
            "NULL" => false
        ),
        "edi_ubi" => Array(
            "nombre" => "Edificación",
            "longitud" => "5",
            "NULL" => false
        ),
        "uni_inm_ubi" => Array(
            "nombre" => "Unidad Inmobiliaria",
            "longitud" => "5",
            "NULL" => false
        )
    );
    
    public function listarUbigeos()
    {          
        $data = Ubigeo::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarUbigeo($id)
    {       
        $tip_arm = Ubigeo::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarUbigeo($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , Ubigeo::$validaciones);
        if($respuesta === true)
        {            
            if(!Ubigeo::find_by_ent_fed_ubi_and_mun_ubi_and_parr_ubi_and_seg_sec_ubi_and_cen_pob_ubi_and_man_ubi_and_parc_ubi_and_edi_ubi_and_uni_inm_ubi($datos["ent_fed_ubi"],$datos["mun_ubi"],$datos["parr_ubi"],$datos["seg_sec_ubi"],$datos["cen_pob_ubi"],$datos["man_ubi"],$datos["parc_ubi"],$datos["edi_ubi"],$datos["uni_inm_ubi"]))
            {
                if($id_ubi = Ubigeo::create($datos))
                {
                    return Herramientas::obtener_atributos($id_ubi)->id_ubi;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarUbigeo($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , Ubigeo::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = Ubigeo::find($datos["id_ubi"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarUbigeo($id)
    {             
        try 
        {
            $tip_arm = Ubigeo::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
