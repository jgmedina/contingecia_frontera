<?php
class SectorEconomico extends ActiveRecord\Model
{
    static $table_name = 'sector_economico';    
    static $primary_key = 'id_sec_eco';    
    static $validaciones = Array(
        "id_uni_eco" => Array(
            "nombre" => "Identificador de la Unidad Económica",
            "longitud" => "255",
            "NULL" => false
        ),
        "cod_ciiu" => Array(
            "nombre" => "Código CIIU",
            "longitud" => "4",
            "NULL" => false
        ),
        "des_sec_eco" => Array(
            "nombre" => "Descripción",
            "longitud" => "255",
            "NULL" => false
        )
    );
    
    public function agregarSectorEconomico($datos)
    {                
        $respuesta = Herramientas::validaciones($datos , SectorEconomico::$validaciones);
        if($respuesta === true)
        {
            if(!SectorEconomico::find_by_cod_ciiu($datos["cod_ciiu"]))
            {
                if(SectorEconomico::create($datos))
                {
                    return TRUE;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
}
?>
