<?php
class NivelActividad extends ActiveRecord\Model
{
    static $table_name = 'nivel_actividad';    
    static $primary_key = 'id_niv_act';    
    static $validaciones = Array(
        "est_niv_act" => Array(
            "nombre" => "Estatus",
            "longitud" => "10",
            "NULL" => false
        ),
        "en_con_niv_act" => Array(
            "nombre" => "En Construcción",
            "longitud" => "10",
            "NULL" => false
        ),
        "obs_niv_act" => Array(
            "nombre" => "Observaciones",
            "longitud" => "255",
            "NULL" => false
        )
    );
    
    public function listarNivelActividads()
    {          
        $data = NivelActividad::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarNivelActividad($id)
    {       
        $tip_arm = NivelActividad::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarNivelActividad($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , NivelActividad::$validaciones);
        if($respuesta === true)
        {
            if($id_niv_act = NivelActividad::create($datos))
            {
                return Herramientas::obtener_atributos($id_niv_act)->id_niv_act;//exito
            }
            else
            {
                return 0;//Error
            }            
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarNivelActividad($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , NivelActividad::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = NivelActividad::find($datos["id_niv_act"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarNivelActividad($id)
    {             
        try 
        {
            $tip_arm = NivelActividad::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
