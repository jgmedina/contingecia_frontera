<?php
class Permiso extends ActiveRecord\Model
{

    //public function listarPermisos($campo = false , $valor = false , $estricto = true , $order = false)
    public function listarPermisos($campo = false , $valor = false , $estricto = true , $order = false)
    {        
        $permisos = Permiso::all(array('conditions' => "nom_per <> 'index'"));
        $datos = Array();
        foreach ($permisos as $value) 
        {
            array_push($datos, (object)$value->attributes());
        }
        if(count($datos) > 0)        
        {                        
            return (object)$datos;
        }
        else
        {
            return FALSE;
        }     
    }
    
    public function selectPermisos()
    {
        $sql ="SELECT DISTINCT ON (nom_per) 
                    id_per , 
                    split_part(fk_per,'_',1) as nom_per
                FROM 
                    permisos
                WHERE 
                    nom_per <> 'index'";        
        
        return Permiso::find_by_sql($sql);
    }    
}
?>
