<?php
class PlanillaInfraesctructura extends ActiveRecord\Model
{
    static $table_name = 'planillas_infraesctructuras';    
    static $primary_key = 'id_pla_inf';    
    static $validaciones = Array(
        "id_pla" => Array(
            "nombre" => "Identificador de Planilla",
            "NULL" => false
        ),
        "id_inf" => Array(
            "nombre" => "Identificador de Infraestructura",
            "NULL" => false
        ),
        "can_pla_inf" => Array(
            "nombre" => "Infraestructura",
            "NULL" => false
        ),
        "est_pla_inf" => Array(
            "nombre" => "Estado de Consevación",
            "longitud" => "1",
            "NULL" => false
        )
    );
    
    public function listarPlanillaInfraesctructuras()
    {          
        $data = PlanillaInfraesctructura::find('all');
        return Herramientas::obtener_atributos($data);
    }
    
    public function consultarPlanillaInfraesctructura($id)
    {       
        $tip_arm = PlanillaInfraesctructura::find($id); 
        return (object)$tip_arm->attributes();
    }
    
    public function agregarPlanillaInfraesctructura($datos)
    {        
        $respuesta = Herramientas::validaciones($datos , PlanillaInfraesctructura::$validaciones);        
        if($respuesta === true)
        {                        
            if(!PlanillaInfraesctructura::find_by_id_pla_and_id_inf($datos["id_pla"],$datos["id_inf"]))
            {
                if(PlanillaInfraesctructura::create($datos))
                {
                    return true;//exito
                }
                else
                {
                    return 0;//Error
                }            
            }
            else
            {
                return -1;//existe
            }
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function modificarPlanillaInfraesctructura($datos)
    {   
        $respuesta = Herramientas::validaciones($datos , PlanillaInfraesctructura::$validaciones);
        if($respuesta === true)
        {
            $tip_arm = PlanillaInfraesctructura::find($datos["id_inf"]);
            return ($tip_arm->update_attributes($datos))?TRUE:FALSE;
        }
        else
        {
            return $respuesta;
        }
    }
    
    public function eliminarPlanillaInfraesctructura($id)
    {             
        try 
        {
            $tip_arm = PlanillaInfraesctructura::find($id);
        }
        catch (ActiveRecord\RecordNotFound $e) 
        {
            return FALSE;
            exit();
        }
        $tip_arm->delete();
        
        return TRUE;
    }          
}
?>
