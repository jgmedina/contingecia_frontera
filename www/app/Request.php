<?php
 
class Request
{
    private $_controlador;
    private $_metodo;
    private $_argumentos;
    private $_validaciones;    
    private $_data;    

    public function __construct()
    {   
        if(isset($_GET['url']))
        {
            $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);                        
            $url = explode('/', $url);            
            $url = array_filter($url);
            
            $this->_controlador = strtolower(array_shift($url));            
            $this->_metodo = strtolower(array_shift($url));            
            $this->_argumentos = $url;            
        }
        
        if(!$this->_controlador)
        {
            $this->_controlador = DEFAULT_CONTROLLER;            
        }
 
        if(!$this->_metodo)
        {
            $this->_metodo = 'index';
        }
 
        if(!isset($this->_argumentos))
        {
            $this->_argumentos = array();
        }    
    }
 
    public function getControlador()
    {
        if(!Session::get('acl'))
        {
            $this->_controlador = DEFAULT_CONTROLLER;            
        }

        return $this->_controlador;
    }
 
    public function getMetodo()
    {        
        if(!Session::get('acl'))
        {
            $this->_metodo = 'index';
        }
        return $this->_metodo;
    }
 
    public function getArgumentos()
    {
        if(!Session::get('acl'))
        {
            $this->_argumentos = array();
        }    
        return $this->_argumentos;
    }
    
    public function getData($datos)
    {        
        if(is_array($datos))
        {
            foreach ($datos as $key => $value) 
            {                
                if(is_array($value))
                {
                    foreach ($value as $k => $v) 
                    {
                        if(is_array($v))
                        {
                            $v = array_map('addslashes', $v);
                            $v = array_map('trim', $v);
                            $respuesta[$key][$k] = $v;
                        }
                        else
                        {
                            $respuesta[$key][$k] = trim(addslashes($v));
                        }
                        
                        
                    }
                }
                else
                {
                    $respuesta[$key] = trim(addslashes($value));
                }
            }
        }
        else 
        {
            $respuesta = addslashes($datos);
        }       
        
        return $respuesta;
    }
    
    public function getValidador()
    {        
        $explorador = new Explorador($this->_controlador, $this->_metodo);
        if($this->_validaciones = $explorador->_validaciones)
        {
            
            return $this->_validaciones;
        }
        else
        {
            return false;
        }
        
    }
}
?>