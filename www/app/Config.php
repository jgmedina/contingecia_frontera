<?php
/**
 * Base para cargar las demás vistas
 */ 
define('BASE_URL', 'http://'.$_SERVER["SERVER_NAME"].'/contingencia_frontera/www/');
//define('BASE_URL', 'http://'.$_SERVER["SERVER_NAME"].'/');

/**
 * Controlador por default
 */ 
define('DEFAULT_CONTROLLER', 'index');
/**
 * Layout por default
 */ 
define('DEFAULT_LAYOUT', 'default'); // Está será la plantilla seleccionada
/**
 * Nombre del site
 */ 
define('APP_NOMBRE', 'Catastro Industrial');
/**
 * Nombre del site
 */ 
define('APP_ABREVIADO', 'I.G.V.S.B');
/**
 * Nombre del site
 */ 
define('APP_SUBNOMBRE', 'Catastro Industrial');
/**
 * CopyRight de nuestro site
 */ 
define('APP_COMPANY', '');
/**
 * Define Formato de fecha POSTGRES
 */ 
define('FORMAT_FECHA_PG', 'dd/mm/yyyy');
/**
 * Define Formato de fecha
 */ 
define('FORMAT_FECHA', 'd/m/Y');
/**
 * Define Formato de Hora
 */ 
define('FORMAT_HORA', 'hh:mm:ss');
/**
 * Mostrar Barra superior
 */
//define('BARRA_SUP', TRUE);

/**
 * Mostrar header
 */
//define('HEADER', TRUE);
/**
 * Nombre del logo
 */
define('NOMBRE_LOGO', "default");
/**
 * Nombre del TEMA
 */
//define('NOMBRE_TEMA', "Yeti");
//define('NOMBRE_TEMA', "United");
//define('NOMBRE_TEMA', "Superhero");
//define('NOMBRE_TEMA', "Spacelab");
//define('NOMBRE_TEMA', "Slate");
define('NOMBRE_TEMA', "Simplex");
//define('NOMBRE_TEMA', "Readable");
//define('NOMBRE_TEMA', "Lumen");
//define('NOMBRE_TEMA', "Journal");
//define('NOMBRE_TEMA', "Flatly");
//define('NOMBRE_TEMA', "Darkly");
//define('NOMBRE_TEMA', "Cybord");
//define('NOMBRE_TEMA', "Cosmo");
//define('NOMBRE_TEMA', "Cerulean");
/**
 * Módulos javascript's
 */ 
define('ruta_lib' , BASE_URL . 'libs/');

define("LIBRERIAS", serialize(
        Array(
            "nprogress" => '<link href="'.ruta_lib.'nprogress/nprogress.css" rel="stylesheet" type="text/css"/>'
            . '<script src="'.ruta_lib.'nprogress/nprogress.js"></script>',
            "datatables" => '<link href="'.ruta_lib.'DataTables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />'
            . '<script src="'.ruta_lib.'DataTables/js/jquery.dataTables.min.js"></script>'
            . '<script src="'.ruta_lib.'DataTables/js/dataTables.bootstrap.js"></script>',
            "openlayers" => '<script src="'.ruta_lib.'OpenLayers-2.13.1/OpenLayers.js"></script>',
            "highcharts" => '<script src="'.ruta_lib.'Highcharts/highcharts.js"></script>'
            . '<script src="'.ruta_lib.'Highcharts/themes/grid-light.js"></script>'
            . '<script src="'.ruta_lib.'Highcharts/modules/drilldown.js"></script>'
            . '<script src="'.ruta_lib.'Highcharts/modules/exporting.js"></script>'            
            )
        )
    );
define("MODULOS_LIBRERIAS", serialize(
        Array(
            "index"=>Array("nprogress","highcharts","datatables"),
            "usuarios"=>Array("nprogress","datatables"),
            "roles"=>Array("nprogress","datatables"),
            "permisos"=>Array("nprogress","datatables"),
            "formularios"=>Array("nprogress","datatables"),
            "construccion"=>Array("nprogress"),
            "contrasena"=>Array("nprogress"),
            "unidadeseconomica"=>Array("nprogress"),
            "unidadesmedidas"=>Array("nprogress","datatables")
            )
        )
    );                             

/**
 * Parametros para la conexión a base de datos
 */
define('DB_HOST', 'predator05');
define('DB_USER', 'postgres');
define('DB_PASS', 'postgres');
define('DB_NAME', 'contingencia_frontera');
define('DB_PORT', '5432');
define('DB_PREFIX', '');

define('DB_PG_CONNECTION', 'pgsql://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME);

?>
