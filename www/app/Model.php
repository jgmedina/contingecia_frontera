<?php
/**
* Clase base del modelo del framework
*
* @package    Clase Modelo
* @author     José Medina
* @copyright Instituto Geográfico de Venezuela Simón Bolivar
*/
class Model 
{
	protected $_db;
        
        public function __construct()
	{
            $this->_db = new Database();
	}
}
?>
