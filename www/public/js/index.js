Highcharts.setOptions({
    global: {
        useUTC: true
    },
    lang: {
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        shortMonths: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],//['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdays: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
        resetZoom: "Reiniciar Zoon",
        drillUpText: '<< Regresar a {series.name}'
    }
});


var opc = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        width: 867,
        height:290
    },                            
    credits:{
        enabled: false
    },
    tooltip : {
        pointFormat: '{point.name}: <b>{point.y}%</b>'
    },
    plotOptions : {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format:'{point.name}: <b>{point.y}%</b>'
            },
            showInLegend: true
        }
    },
    series : {}
};

var total = new Array();

function iniciarValor()
{
    total = new Array();
}

function sumarValor(valor,indice)
{    
    //console.log(valor);
    //console.log(indice);
    valor = valor.split(".").join("");
    valor= valor.replace(",",".");
    if(typeof(indice) == "undefined")
    {
        return false;
    }
        
    if(typeof(total[indice]) == "undefined")
    {
        total[indice] = 0;
    }
    total[indice] = parseFloat(total[indice]) + parseFloat(valor);
    return total[indice];
}

function formatValor(valor)
{
    valor = valor.toString().replace(/./g, function(c, i, a)
    {
        return i && c !== "," && ((a.length - i) % 3 === 0) ? '.' + c : c;
    }); 
    return valor;
}

function cargaMunicipio(obj , selector1 , selector2)
{
    selector2 = (typeof(selector2) != "undefined")?selector2:false;
    var $contenedor = $(obj).parents(".row").eq(0);
    var url = base_url+"municipios/ajaxListarMunicipios/"+obj.value;
    cargarDatosEntidad(obj.value);
    cargando();
    var exito = function(datos){           
        
        $contenedor.find("#"+selector1).empty();
        $contenedor.find("#"+selector1).append("<option value='0'>Seleccionar</option>");

        if(datos.length != 0)
        {
            $.each(datos,function(i,e){
                
                $contenedor.find("#"+selector1).append("<option value='"+e.id_mun+"'>"+e.nom_mun+"</option>");
            });
            $contenedor.find("#"+selector1).removeAttr("disabled");
            $contenedor.find("#"+selector1).focus();
        }
        else
        {                         
            if (selector2)
            {
                $contenedor.find("#"+selector2).val(0);
                $contenedor.find("#"+selector2).attr("disabled","disabled");
            }
            $contenedor.find("#"+selector1).attr("disabled","disabled");
        }
    };
    peticionAjax(url , {}, "POST" , "json" , exito);                
}

function cargaCircunscripcion(obj , selector1 , selector2)
{
    selector2 = (typeof(selector2) != "undefined")?selector2:false;
    var $contenedor = $(obj).parents(".row").eq(0);
    var url = base_url+"circunscripciones/ajaxListarCircunscripciones/"+obj.value;    
    if(obj.value != 0)
    {
        $("#nom_ent").text($(obj).find("option:selected").text());
    }
    else
    {
        $("#nom_ent").text("");
    }
    //
    cargarDatosCircunscripcion(obj.value);
    cargarDatosEntidad(obj.value);
    cargando();
    var exito = function(datos){            
        $contenedor.find("#"+selector1).empty();
        $contenedor.find("#"+selector1).append("<option value='0'>Seleccionar</option>");            
        
        if(datos.length != 0)
        {
            $.each(datos,function(i,e){
                $contenedor.find("#"+selector1).append("<option value='"+e.id_cir+"'>"+e.nom_cir+"</option>");
            });
            $contenedor.find("#"+selector1).removeAttr("disabled");                                
            //$contenedor.find("#"+selector1).focus();
        }
        else
        {            
            if(selector2)
            {
                $contenedor.find("#"+selector2).val(0);
                $contenedor.find("#"+selector2).attr("disabled","disabled");
            }
            $contenedor.find("#"+selector1).attr("disabled","disabled");
        }

    };
    peticionAjax(url , {}, "POST" , "json" , exito);
}

function cargarDatosEntidad(cod_ent)
{
    if(cod_ent != 0 )
    {                
        var url = base_url+"entidades/ajaxConsultarEntidad/"+cod_ent;    
        cargando();
        var exito = function(datos){           
            $("#nom_gob").text(datos.nom_gob);
            $("#pos_pol_gob").text(datos.pos_pol_gob);
            cargaTablas();
        };
        peticionAjax(url , {}, "POST" , "json" , exito);                
    }
    else
    {
        $("#nom_gob").text("");
        $("#pos_pol_gob").text("");
        
    }
}

function cargarDatosCircunscripcion(cod_ent)
{
    if(cod_ent != 0 )
    {        
        //var url = base_url+"circunscripciones/ajaxConsultarCircunscripcion/"+cod_ent+"/"+id_mun;    
        var url = base_url+"circunscripciones/ajaxConsultarCircunscripcion/"+cod_ent;    
        cargando();
        var exito = function(datos){                       
            $("#num_par").text(datos.tab1.num_par);
            $("#num_cir").text(datos.tab1.num_cir);
            $("#num_mun").text(datos.tab1.num_mun);
            //$("#inf_pob").text("Censo 2011: "+datos.tab1.inf_pob.pob_2011+"\n Proyección Dic 2015: "+datos.tab1.inf_pob.pob_2015);
            $("#inf_pob").text("Censo 2011: "+datos.tab1.inf_pob.pob_2011);
            //cargaTablas();
        };
        peticionAjax(url , {}, "POST" , "json" , exito);                
    }
    else
    {
        $("#num_par").text("");
        $("#num_cir").text("");
        $("#num_mun").text("");
        $("#nom_gob").text("");
        $("#pos_pol_gob").text("");
        $("#inf_pob").text("");
    }
}

function cargaTablas()
{    
    $("div[class*='tabla']").addClass("hidden");
    var cod_ent = $("#cod_ent").val();
    var id_cir = $("#id_cir").val();
    var tip_dat = $("#tip_dat").val();
    $(".tabbable").find(".nav-tabs,.tab-content").empty();        
    
    if(tip_dat != 0)
    {
        if(cod_ent == 0)
            return false;
                
        switch (tip_dat)
        {
            case "1":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarPoblacion/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarPoblacion/"+cod_ent;
                }
                
                opc.title = {text: 'Población'};
                
                var exito = function(datos){            
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var html = "";
                    var hombres = 0;
                    var mujeres = 0;
                    var t1 = 0;
                    var t2 = 0;
                    var t3 = 0;
                    var t4 = 0;
                    var c = 1;
                    iniciarValor();
                    $.each(datos,function(){
                        html += "<tr>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.cod_cir+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.pob_2011+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.pob_2015+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.pob_muj+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.pob_hom+"</td>";
                        html += "</tr>";
                        
                        t1 = sumarValor(this.pob_2011 , 't1');
                        t2 = sumarValor(this.pob_2015 , 't2');
                        t3 = sumarValor(this.pob_muj , 't3');
                        t4 = sumarValor(this.pob_hom , 't4');

                        hombres = (hombres + parseFloat((this.pob_hom * 100)/this.pob_2011));
                        mujeres = (mujeres + parseFloat((this.pob_muj * 100)/this.pob_2011));                        
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);
                        
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Hombre',
                                        y: parseFloat(((this.pob_hom * 100)/this.pob_2011).toFixed(2)),
                                        color: '#1016FF'
                                    },
                                    {
                                        name: 'Mujer',
                                        y: parseFloat(((this.pob_muj * 100)/this.pob_2011).toFixed(2)),
                                        color: '#F45B5B'
                                    }
                                ]
                            }
                        ];
                    
                    var selector = 'grafico_'+c;                    
                    cargarGrafico(selector , opc);
                    c++;
                    });
                    
                    var divisor = (c-1);
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: [
                                {
                                    name: 'Hombre',
                                        y: parseFloat((hombres/divisor).toFixed(2)),
                                    color: '#1016FF'
                                },
                                {
                                    name: 'Mujer',
                                    y: parseFloat((mujeres/divisor).toFixed(2)),
                                    color: '#F45B5B'
                                }
                            ]
                        }
                    ];
                    cargarGrafico('grafico_0' , opc);
                    
                    var foot = '<th class="text-center">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['t1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['t2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['t3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['t4'])+'</th>';
                    $("#tab1").next("tfoot").empty();
                    $("#tab1").next("tfoot").append('<tr>'+foot+'</tr>');
                    
                    $("#tab1").empty();
                    $("#tab1").append(html);
                    
                };
                
                break;
                
            case "2":
                var url = base_url+"circunscripciones/ajaxConsultarObservaciones/"+cod_ent;
                
                var exito = function(datos){                      
                    agregarGrafico(0 , "General",true);
                    var html = "";
                    var t1 = 0;
                    var c = 1;
                    var _total = new Array();
                    var categoria = new Array();
                    iniciarValor();
                    opc.title = {text: 'OBSERVACIÓN'};
                    opc.plotOptions = {};
                    opc.tooltip = {};                            
                    opc.chart.type = 'column';                    
                    $.each(datos,function(){
                        html += "<tr>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.cod_cir+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.per_ref+"</td>";
                        html += "</tr>";
                        t1 = sumarValor(this.per_ref , 't1');
                        
                        _total.push(parseFloat(this.per_ref));
                        categoria.push(this.cod_cir);
                        
                        opc.xAxis = {categories:[this.cod_cir]};
                        opc.series = [
                            {
                                name: 'Personas en Refugio',
                                data: parseFloat(this.per_ref)
                            }
                        ];
                        
                        var selector = 'grafico_'+c;                    
                        c++;
                    });
                    
                    opc.xAxis = {categories:categoria};
                    opc.series = [
                        {
                            name: 'Personas en Refugio',
                            data: _total
                        }
                    ];
                    cargarGrafico('grafico_0' , opc);
                    
                    $("#tab2").empty();
                    $("#tab2").append(html);
                    
                    var foot = '<th class="text-center">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['t1'])+'</th>';
                    $("#tab2").next("tfoot").empty();
                    $("#tab2").next("tfoot").append('<tr>'+foot+'</tr>');
                };
                break;
                
            case "3":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarPobreza/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarPobreza/"+cod_ent;
                }
                var exito = function(datos){                     
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    opc.title = {text: 'HOGARES EN PROBREZA'};
                    
                    var html = "";
                    var c = 1;
                    var t0 = 0;
                    var t1 = 0;
                    var p1 = 0;
                    var t2 = 0;
                    var p2 = 0;
                    iniciarValor();
                    $.each(datos,function(){
                        html += "<tr>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.cod_cir+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.tot_hog+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.hog_pob+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.hog_pob_por+"%</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.hog_pob_ext+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.hog_pob_ext_por+"%</td>";
                        html += "</tr>";
                        
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);
                        
                        this.hog_pob_por = this.hog_pob_por.replace(",",".");
                        this.hog_pob_ext_por = this.hog_pob_ext_por.replace(",",".");
                        
                        t0 = sumarValor(this.tot_hog , 't0');
                        t1 = sumarValor(this.hog_pob , 't1');
                        p1 = (p1 + parseFloat(this.hog_pob_por));
                        t2 = sumarValor(this.hog_pob_ext , 't2');
                        p2 = (p2 + parseFloat(this.hog_pob_ext_por));
                       
                        opc.series = [
                                {
                                    type: 'pie',
                                    colorByPoint: true,
                                    data: [
                                        {
                                            name: 'Hogares en Pobreza',
                                            y: parseFloat(this.hog_pob_por),
                                            color: '#A9FF97'
                                        },
                                        {
                                            name: 'Hogares en Pobreza Extrema',
                                            y: parseFloat(this.hog_pob_ext_por),
                                            color: '#F45B5B'
                                        }
                                    ]
                                }
                            ];
                        var selector = 'grafico_'+c;
                        cargarGrafico(selector , opc);
                        c++;
                    });
                    var divisor = (c-1);
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: [
                                {
                                    name: 'Hogares en Pobreza',
                                    y: parseFloat((t1/divisor).toFixed(2)),
                                    color: '#A9FF97'
                                },
                                {
                                    name: 'Hogares en Pobreza Extrema',
                                    y: parseFloat((t2/divisor).toFixed(2)),
                                    color: '#F45B5B'
                                }
                            ]
                        }
                    ];
                    cargarGrafico('grafico_0' , opc);
                    $("#tab3").empty();
                    $("#tab3").append(html);
                    
                    var foot = '<th class="text-center">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['t0'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['t1'])+'</th>';
                    foot += '<th class="text-center">'+p1.toFixed(2).replace('.',',')+'%</th>';
                    foot += '<th class="text-center">'+formatValor(total['t2'])+'</th>';
                    foot += '<th class="text-center">'+p2.toFixed(2).replace('.',',')+'%</th>';
                    $("#tab3").next("tfoot").empty();
                    $("#tab3").next("tfoot").append('<tr>'+foot+'</tr>');
                };
                break;
                
            case "4":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarNecesidades/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarNecesidades/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}
                    
                    var theader = "<th class='text-center' class='text-center'>Variables NBI</th>";
                    var c = 1;
                    var t1 = 0; 
                    var t2 = 0; 
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab4_header").empty();
                    $("#tab4_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Hogares con deficit de educación",
                        "Hogares con Déficit según Hacinamiento",
                        "Hogares con Déficit según Calidad de la Vivienda",
                        "Hogares con Déficit de Servicios",
                        "Hogares con Déficit de Capacidad Económica y Educativa de el(la) Jefe(a)"
                    );
                    
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){                            
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].hog_def_educacion , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_def_educacion_por , 'p1');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_educacion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_educacion_por+"%</td>";                                    
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].hog_def_hacinamiento , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_def_hacinamiento_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_hacinamiento+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_hacinamiento_por+"%</td>";
                                break;
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].hog_def_calidad_vivienda , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_def_calidad_vivienda_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_calidad_vivienda+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_calidad_vivienda_por+"%</td>";
                                break;
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].hog_def_servicios , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_def_servicios_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_servicios+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_servicios_por+"%</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].hog_def_cap_eco_edu_jefe , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_def_cap_eco_edu_jefe_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_cap_eco_edu_jefe+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_def_cap_eco_edu_jefe_por+"%</td>";
                                break;                                
                            }  
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'NECESIDADES BASICAS INSATISFECHAS'};
                    $.each(datos.tbody,function(j,e){               
                        var p1 = 0;
                        var p2 = 0;
                        var p3 = 0;
                        var p4 = 0;
                        var p5 = 0;                    
                        
                        this.hog_def_educacion_por = this.hog_def_educacion_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.hog_def_educacion_por));                                    
                        this.hog_def_hacinamiento_por = this.hog_def_hacinamiento_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.hog_def_hacinamiento_por));
                        this.hog_def_calidad_vivienda_por = this.hog_def_calidad_vivienda_por.replace(",",".");
                        p3 = (p3 + parseFloat(this.hog_def_calidad_vivienda_por));
                        this.hog_def_servicios_por = this.hog_def_servicios_por.replace(",",".");
                        p4 = (p4 + parseFloat(this.hog_def_servicios_por));
                        this.hog_def_cap_eco_edu_jefe_por = this.hog_def_cap_eco_edu_jefe_por.replace(",",".");
                        p5 = (p5 + parseFloat(this.hog_def_cap_eco_edu_jefe_por));
                        
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Hogares con deficit de educación',
                                        y: p1
                                    },
                                    {
                                        name: 'Hogares con Déficit según Hacinamiento',
                                        y: p2
                                    },
                                    {
                                        name: 'Hogares con Déficit según Calidad de la Vivienda',
                                        y: p3
                                    },
                                    {
                                        name: 'Hogares con Déficit de Servicios',
                                        y: p4
                                    },
                                    {
                                        name: 'Hogares con Déficit de Capacidad Económica y Educativa de el(la) Jefe(a)',
                                        y: p5
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc);  
                    });
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                                                    
                        cargarGrafico('grafico_0' , opc); 
                        
                    $("#tab4").empty();
                    $("#tab4").append("<tr>"+tbody+"</tr>");
                };
                break;
                
            case "5":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTenencias/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTenencias/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Tenencia Del Hogar</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab5_header").empty();
                    $("#tab5_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Hogar con Vivienda Propia",
                        "Hogares con Vivienda Propia No Propia (Alquilada,Prestada, Cedida, Otra)"                    
                    );
            
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].hog_viv_pro , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_viv_pro_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_viv_pro+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_viv_pro_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].hog_viv_no_pro , 't1');
                                    t2 = sumarValor(datos.tbody[j].hog_viv_no_pro_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_viv_no_pro+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hog_viv_no_pro_por+"%</td>";
                                break;                                
                            }       
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                            
                        });
                        
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'TENENCIA DEL HOGAR'};
                    $.each(datos.tbody,function(j,e){               
                        var p1 = 0;
                        var p2 = 0;
                    
                        this.hog_viv_pro_por = this.hog_viv_pro_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.hog_viv_pro_por));                                    
                        this.hog_viv_no_pro_por = this.hog_viv_no_pro_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.hog_viv_no_pro_por));
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Hogar con Vivienda Propia',
                                        y: p1
                                    },
                                    {
                                        name: 'Hogares con Vivienda Propia No Propia (Alquilada,Prestada, Cedida, Otra)',
                                        y: p2
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab5").empty();
                    $("#tab5").append(tbody);
                };
                break;
                
            case "6":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTipoVivienda/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTipoVivienda/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Tipos de Viviendas</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });
                            theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab6_header").empty();
                    $("#tab6_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                    "Quinta o casaquinta",
                    "Casa",                    
                    "Apartamento en edificio",                    
                    "Apartamento en quinta, casaquinta o casa",                    
                    "Casa de vecindad",                    
                    "Rancho",                    
                    "Refugio",                    
                    "Vivienda indígena",                    
                    "Otra clase",                    
                    "Total de Viviendas de uso colectivo voluntario"                    
                    );
                    
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].quinta , 't1');
                                    t2 = sumarValor(datos.tbody[j].quinta_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].quinta+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].quinta_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].casa , 't1');
                                    t2 = sumarValor(datos.tbody[j].casa_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].casa+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].casa_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].apto_edificio , 't1');
                                    t2 = sumarValor(datos.tbody[j].apto_edificio_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].apto_edificio+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].apto_edificio_por+"%</td>";
                                break;                                
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].apto_quinta , 't1');
                                    t2 = sumarValor(datos.tbody[j].apto_quinta_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].apto_quinta+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].apto_quinta_por+"%</td>";
                                break;                                
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].casa_vecindad , 't1');
                                    t2 = sumarValor(datos.tbody[j].casa_vecindad_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].casa_vecindad+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].casa_vecindad_por+"%</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].rancho , 't1');
                                    t2 = sumarValor(datos.tbody[j].rancho_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].rancho+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].rancho_por+"%</td>";
                                break;                                
                                case 6:
                                    t1 = sumarValor(datos.tbody[j].refugio , 't1');
                                    t2 = sumarValor(datos.tbody[j].refugio_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].refugio+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].refugio_por+"%</td>";
                                break;                                
                                case 7:
                                    t1 = sumarValor(datos.tbody[j].vivienda_indigena , 't1');
                                    t2 = sumarValor(datos.tbody[j].vivienda_indigena_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].vivienda_indigena+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].vivienda_indigena_por+"%</td>";
                                break;                                
                                case 8:
                                    t1 = sumarValor(datos.tbody[j].otra_clase , 't1');
                                    t2 = sumarValor(datos.tbody[j].otra_clase_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otra_clase+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otra_clase_por+"%</td>";
                                break;                                
                                case 9:
                                    t1 = sumarValor(datos.tbody[j].viv_uso_colectivo_voluntario_involuntario , 't1');
                                    t2 = sumarValor(datos.tbody[j].viv_uso_colectivo_voluntario_involuntario_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_uso_colectivo_voluntario_involuntario+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_uso_colectivo_voluntario_involuntario_por+"%</td>";
                                break;                                
                            }               
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'TIPO DE VIVIENDA'};
                    $.each(datos.tbody,function(j,e){               
                        var p1 = 0;var p2 = 0;
                        var p3 = 0;var p4 = 0;
                        var p5 = 0;var p6 = 0;
                        var p7 = 0;var p8 = 0;
                        var p9 = 0;var p10 = 0;
                        
                        this.quinta_por = this.quinta_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.quinta_por));                                    
                        this.casa_por = this.casa_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.casa_por));                        
                        this.apto_edificio_por = this.apto_edificio_por.replace(",",".");
                        p3 = (p3 + parseFloat(this.apto_edificio_por));
                        this.apto_quinta_por = this.apto_quinta_por.replace(",",".");
                        p4 = (p4 + parseFloat(this.apto_quinta_por));
                        this.casa_vecindad_por = this.casa_vecindad_por.replace(",",".");
                        p5 = (p5 + parseFloat(this.casa_vecindad_por));
                        this.rancho_por = this.rancho_por.replace(",",".");
                        p6 = (p6 + parseFloat(this.rancho_por));
                        this.refugio_por = this.refugio_por.replace(",",".");
                        p7 = (p7 + parseFloat(this.refugio_por));
                        this.vivienda_indigena_por = this.vivienda_indigena_por.replace(",",".");
                        p8 = (p8 + parseFloat(this.vivienda_indigena_por));
                        this.otra_clase_por = this.otra_clase_por.replace(",",".");
                        p9 = (p9 + parseFloat(this.otra_clase_por));
                        this.viv_uso_colectivo_voluntario_involuntario_por = this.viv_uso_colectivo_voluntario_involuntario_por.replace(",",".");
                        p10 = (p10 + parseFloat(this.viv_uso_colectivo_voluntario_involuntario_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Quinta o casaquinta',
                                        y: p1
                                    },
                                    {
                                        name: 'Casa',
                                        y: p2
                                    },
                                    {
                                        name: 'Apartamento en edificio',
                                        y: p3
                                    },
                                    {
                                        name: 'Apartamento en quinta, casaquinta o casa',
                                        y: p4
                                    },
                                    {
                                        name: 'Casa de vecindad',
                                        y: p5
                                    },
                                    {
                                        name: 'Rancho',
                                        y: p6
                                    },
                                    {
                                        name: 'Refugio',
                                        y: p7
                                    },
                                    {
                                        name: 'Vivienda indígena',
                                        y: p8
                                    },
                                    {
                                        name: 'Otra clase',
                                        y: p9
                                    },
                                    {
                                        name: 'Total de Viviendas de uso colectivo voluntario',
                                        y: p10
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab6").empty();
                    $("#tab6").append(tbody);
                };
                break;
                
            case "7":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarSituacionVivienda/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarSituacionVivienda/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Situación de Viviendas</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab7_header").empty();
                    $("#tab7_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Viviendas Construidas",
                        "Viviendas en Construcción"
                    );
                    
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].viv_construccion , 't1');
                                    t2 = sumarValor(datos.tbody[j].viv_construccion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_construccion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_construccion_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].viv_en_construccion , 't1');
                                    t2 = sumarValor(datos.tbody[j].viv_en_construccion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_en_construccion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].viv_en_construccion_por+"%</td>";
                                break; 
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].hab_viv , 't1');
                                    t2 = sumarValor(datos.tbody[j].hab_viv_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hab_viv+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hab_viv_por+"%</td>";
                                break;                                                                                              
                            }      
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                            
                        });
                        
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'SITUACION DE LAS VIVIENDAS'};
                    
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;
                        
                        this.viv_construccion_por = this.viv_construccion_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.viv_construccion_por));                                    
                        this.viv_en_construccion_por = this.viv_en_construccion_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.viv_en_construccion_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Viviendas Construidas',
                                        y: p1
                                    },
                                    {
                                        name: 'Viviendas en Construcción',
                                        y: p2
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab7").empty();
                    $("#tab7").append(tbody);
                };
                break;
                
            case "8":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialParedes/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialParedes/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Materiales</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });             
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab8_header").empty();
                    $("#tab8_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Bloque, Ladrillo Frisado",
                        "Bloque, Ladrillo sin Frisar",                    
                        "Obra limpia",
                        "Concreto",
                        "Madera",
                        "pcv",
                        "Bahareque",
                        "Lamina de Zinc, Cartón y/o similares"
                    );
                    
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].bloque_ladrillo_frisado , 't1');
                                    t2 = sumarValor(datos.tbody[j].bloque_ladrillo_frisado_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bloque_ladrillo_frisado+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bloque_ladrillo_frisado_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].bloque_ladrillo_sin_frisar , 't1');
                                    t2 = sumarValor(datos.tbody[j].bloque_ladrillo_sin_frisar_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bloque_ladrillo_sin_frisar+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bloque_ladrillo_sin_frisar_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].obra_limpia , 't1');
                                    t2 = sumarValor(datos.tbody[j].obra_limpia_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].obra_limpia+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].obra_limpia_por+"%</td>";
                                break;                                                                                              
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].concreto , 't1');
                                    t2 = sumarValor(datos.tbody[j].concreto_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].concreto+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].concreto_por+"%</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].madera , 't1');
                                    t2 = sumarValor(datos.tbody[j].madera_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].madera+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].madera_por+"%</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].pvc , 't1');
                                    t2 = sumarValor(datos.tbody[j].pvc_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].pvc+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].pvc_por+"%</td>";
                                break;                                                                                              
                                case 6:
                                    t1 = sumarValor(datos.tbody[j].bahareque , 't1');
                                    t2 = sumarValor(datos.tbody[j].bahareque_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bahareque+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].bahareque_por+"%</td>";
                                break;                                
                                case 7:
                                    t1 = sumarValor(datos.tbody[j].zinc_carton_similares , 't1');
                                    t2 = sumarValor(datos.tbody[j].zinc_carton_similares_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].zinc_carton_similares+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].zinc_carton_similares_por+"%</td>";
                                break;                                                                                              
                            }       
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                            
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'MATERIAL PREDOMINANTE EN PAREDES'};
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;
                        var p3 = 0;
                        var p4 = 0;
                        var p5 = 0;                    
                        var p6 = 0;                    
                        var p7 = 0;                    
                        var p8 = 0;                    
                        
                        this.bloque_ladrillo_frisado_por = this.bloque_ladrillo_frisado_por.replace(",",".");
                        p1 = (t1 + parseFloat(this.bloque_ladrillo_frisado_por));                                    
                        this.bloque_ladrillo_sin_frisar_por = this.bloque_ladrillo_sin_frisar_por.replace(",",".");
                        p2 = (t2 + parseFloat(this.bloque_ladrillo_sin_frisar_por));                        
                        this.obra_limpia_por = this.obra_limpia_por.replace(",",".");
                        p3 = (p3 + parseFloat(this.obra_limpia_por));                        
                        this.concreto_por = this.concreto_por.replace(",",".");
                        p4 = (p4 + parseFloat(this.concreto_por));                        
                        this.madera_por = this.madera_por.replace(",",".");
                        p5 = (p5 + parseFloat(this.madera_por));                        
                        this.pvc_por = this.pvc_por.replace(",",".");
                        p6 = (p6 + parseFloat(this.pvc_por));                        
                        this.bahareque_por = this.bahareque_por.replace(",",".");
                        p7 = (p7 + parseFloat(this.bahareque_por));                        
                        this.zinc_carton_similares_por = this.zinc_carton_similares_por.replace(",",".");
                        p8 = (p8 + parseFloat(this.zinc_carton_similares_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Bloque, Ladrillo Frisado',
                                        y: p1
                                    },
                                    {
                                        name: 'Bloque, Ladrillo sin Frisar',
                                        y: p2
                                    },
                                    {
                                        name: 'Obra limpia',
                                        y: p3
                                    },
                                    {
                                        name: 'Concreto',
                                        y: p4
                                    },
                                    {
                                        name: 'Madera',
                                        y: p5
                                    },
                                    {
                                        name: 'pcv',
                                        y: p6
                                    },
                                    {
                                        name: 'Bahareque',
                                        y: p7
                                    },
                                    {
                                        name: 'Lamina de Zinc, Cartón y/o similares',
                                        y: p8
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab8").empty();
                    $("#tab8").append(tbody);
                };
                break;
                
            case "9":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialTechos/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialTechos/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Materiales</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });     
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab9_header").empty();
                    $("#tab9_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Platabanda",
                        "Teja",                    
                        "Láminas asfálticas",
                        "Láminas metálicas (zinc,aluminio y similares)",
                        "Asbesto y similares",
                        "Láminas de PVC",
                        "Otros (latón,tablas o similares)"
                    );
                    
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].platabanda , 't1');
                                    t2 = sumarValor(datos.tbody[j].platabanda_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].platabanda+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].platabanda_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].teja , 't1');
                                    t2 = sumarValor(datos.tbody[j].teja_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].teja+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].teja_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].lam_asfalticas , 't1');
                                    t2 = sumarValor(datos.tbody[j].lam_asfalticas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_asfalticas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_asfalticas_por+"%</td>";
                                break;                                                                                              
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].lam_metalicas , 't1');
                                    t2 = sumarValor(datos.tbody[j].lam_metalicas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_metalicas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_metalicas_por+"%</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].asbesto , 't1');
                                    t2 = sumarValor(datos.tbody[j].asbesto_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].asbesto+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].asbesto_por+"%</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].lam_pvc , 't1');
                                    t2 = sumarValor(datos.tbody[j].lam_pvc_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_pvc+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].lam_pvc_por+"%</td>";
                                break;                                                                                              
                                case 6:
                                    t1 = sumarValor(datos.tbody[j].otros , 't1');
                                    t2 = sumarValor(datos.tbody[j].otros_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otros+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otros_por+"%</td>";
                                break;                                
                            }               
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'MATERIAL PREDOMINANTE EN TECHO'};
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;
                        var p3 = 0;
                        var p4 = 0;
                        var p5 = 0;          
                        var p6 = 0;          
                        var p7 = 0;          
                        
                        this.platabanda_por = this.platabanda_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.platabanda_por));                                    
                        this.teja_por = this.teja_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.teja_por));                        
                        this.lam_asfalticas_por = this.lam_asfalticas_por.replace(",",".");
                        p3 = (p3 + parseFloat(this.lam_asfalticas_por));                        
                        this.lam_metalicas_por = this.lam_metalicas_por.replace(",",".");
                        p4 = (p4 + parseFloat(this.lam_metalicas_por));                        
                        this.asbesto_por = this.asbesto_por.replace(",",".");
                        p5 = (p5 + parseFloat(this.asbesto_por));                        
                        this.lam_pvc_por = this.lam_pvc_por.replace(",",".");
                        p6 = (p6 + parseFloat(this.lam_pvc_por));                        
                        this.otros_por = this.otros_por.replace(",",".");
                        p7 = (p7 + parseFloat(this.otros_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Platabanda',
                                        y: p1
                                    },
                                    {
                                        name: 'Teja',
                                        y: p2
                                    },
                                    {
                                        name: 'Láminas asfálticas',
                                        y: p3
                                    },
                                    {
                                        name: 'Láminas metálicas (zinc,aluminio y similares)',
                                        y: p4
                                    },
                                    {
                                        name: 'Asbesto y similares',
                                        y: p5
                                    },
                                    {
                                        name: 'Láminas de PVC',
                                        y: p6
                                    },
                                    {
                                        name: 'Otros (latón,tablas o similares)',
                                        y: p7
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab9").empty();
                    $("#tab9").append(tbody);
                };
                break;
                
            case "10":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialPisos/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarMaterialPisos/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Materiales</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    var t3 = 0;
                    var t4 = 0;
                    var t5 = 0;
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });     
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab10_header").empty();
                    $("#tab10_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Mármol, mosaico, granito, vinil, cerámica, ladrillo, terracota, parquet, alfombra y similares",
                        "Cemento",                    
                        "Tierra",
                        "Tablas",
                        "Otros"
                    );
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].varios , 't1');
                                    t2 = sumarValor(datos.tbody[j].varios_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].varios+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].varios_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].cemento , 't1');
                                    t2 = sumarValor(datos.tbody[j].cemento_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].cemento+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].cemento_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].tierra , 't1');
                                    t2 = sumarValor(datos.tbody[j].tierra_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].tierra+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].tierra_por+"%</td>";
                                break;                                                                                              
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].tablas , 't1');
                                    t2 = sumarValor(datos.tbody[j].tablas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].tablas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].tablas_por+"%</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].otros , 't1');
                                    t2 = sumarValor(datos.tbody[j].otros_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otros+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].otros_por+"%</td>";
                                break;                                                                
                            }               
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'MATERIAL PREDOMINANTE EN PISO'};
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;
                        var p3 = 0;
                        var p4 = 0;
                        var p5 = 0;                    
            
                        this.varios_por = this.varios_por.replace(",",".");
                        p1 = (p1 + parseFloat(this.varios_por));                                    
                        this.cemento_por = this.cemento_por.replace(",",".");
                        p2 = (p2 + parseFloat(this.cemento_por));                        
                        this.tierra_por = this.tierra_por.replace(",",".");
                        p3 = (p3 + parseFloat(this.tierra_por));                        
                        this.tablas_por = this.tablas_por.replace(",",".");
                        p4 = (p4 + parseFloat(this.tablas_por));                        
                        this.otros_por = this.otros_por.replace(",",".");
                        p5 = (p5 + parseFloat(this.otros_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Mármol, mosaico, granito, vinil, cerámica, ladrillo, terracota, parquet, alfombra y similares',
                                        y: p1
                                    },
                                    {
                                        name: 'Cemento',
                                        y: p2
                                    },
                                    {
                                        name: 'Tierra',
                                        y: p3
                                    },
                                    {
                                        name: 'Tablas',
                                        y: p4
                                    },
                                    {
                                        name: 'Otros',
                                        y: p5
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab10").empty();
                    $("#tab10").append(tbody);
                };
                break;
                
            case "11":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarServiciosBasicos/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarServiciosBasicos/"+cod_ent;
                }
                
                opc.title = {text: 'ACCESO A SERVICIOS BASICOS'};
                iniciarValor();
                var exito = function(datos){            
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var html = "";
                    var s1 = 0; var n1 = 0;
                    var s2 = 0; var n2 = 0;
                    var s3 = 0; var n3 = 0;
                    var s4 = 0; var n4 = 0;
                    var s5 = 0; var n5 = 0;
                    
                    var c = 1;
                    $.each(datos,function(){
                        html += "<tr>";
                        html += "<td class='text-center' colspan='2'>"+this.cod_cir+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_gas+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_gas+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_acueducto+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_acueducto+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_servicio_electrico+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_servicio_electrico+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_aseo+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_aseo+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_cluacas+"</td>";
                        html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_cluacas+"</td>";
                        html += "</tr>";
                        
                        this.si_gas = this.si_gas.replace(".","");
                        s1 = sumarValor(this.si_gas , 's1');//parseFloat(s1 + parseFloat(this.si_gas));
                        this.no_gas = this.no_gas.replace(".","");
                        n1 = sumarValor(this.no_gas , 'n1');//parseFloat(n1 + parseFloat(this.no_gas));
                        this.si_acueducto = this.si_acueducto.replace(".","");
                        s2 = sumarValor(this.si_acueducto , 's2');//parseFloat(s2 + parseFloat(this.si_acueducto));
                        this.no_acueducto = this.no_acueducto.replace(".","");
                        n2 = sumarValor(this.no_acueducto , 'n2');//parseFloat(n2 + parseFloat(this.no_acueducto));
                        this.si_servicio_electrico = this.si_servicio_electrico.replace(".","");
                        s3 = sumarValor(this.si_servicio_electrico , 's3');//parseFloat(s3 + parseFloat(this.si_servicio_electrico));
                        this.no_servicio_electrico = this.no_servicio_electrico.replace(".","");
                        n3 = sumarValor(this.no_servicio_electrico , 'n3');//parseFloat(n3 + parseFloat(this.no_servicio_electrico));
                        this.si_aseo = this.si_aseo.replace(".","");
                        s4 = sumarValor(this.si_aseo , 's4');//parseFloat(s4 + parseFloat(this.si_aseo));
                        this.no_aseo = this.no_aseo.replace(".","");
                        n4 = sumarValor(this.no_aseo , 'n4');//parseFloat(n4 + parseFloat(this.no_aseo));
                        this.si_cluacas = this.si_cluacas.replace(".","");
                        s5 = sumarValor(this.si_cluacas , 's5');//parseFloat(s5 + parseFloat(this.si_cluacas));
                        this.no_cluacas = this.no_cluacas.replace(".","");
                        n5 = sumarValor(this.no_cluacas , 'n5');//parseFloat(n5 + parseFloat(this.no_cluacas));

                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);
                        
                        opc.tooltip = {};                            
                        opc.plotOptions = {};
                        opc.chart.type = 'column';
                        opc.xAxis = {
                                categories: [
                                    'Servicio de Gas Directo', 
                                    'Servicio de Acueducto o Tubería', 
                                    'Servicio de la Red Publica de Servicio Electrico', 
                                    'Servicio de Aseo Urbano', 
                                    'Servicio de Cloacas'
                                ]
                            };
                        opc.series = [
                                {
                                    name: 'Si',
                                    data: [
                                        parseFloat(this.si_gas),
                                        parseFloat(this.si_acueducto),
                                        parseFloat(this.si_servicio_electrico),
                                        parseFloat(this.si_aseo),
                                        parseFloat(this.si_cluacas)
                                    ]
                                },
                                {
                                    name: 'No',
                                    data: [
                                        parseFloat(this.no_gas),
                                        parseFloat(this.no_acueducto),
                                        parseFloat(this.no_servicio_electrico),
                                        parseFloat(this.no_aseo),
                                        parseFloat(this.no_cluacas)
                                    ]
                                }
                            ];
                            
                            var selector = 'grafico_'+c;
                            cargarGrafico(selector , opc);
                            c++;
                        });
                        
                        opc.series = [
                            {
                                name: 'Si',
                                data: [
                                    s1,s2,s3,s4,s5
                                ]
                            },
                            {
                                name: 'No',
                                data: [
                                    n1,n2,n3,n4,n5
                                ]
                            }
                        ];
                        
                        cargarGrafico('grafico_0' , opc);
                    
                    $("#tab11").empty();
                    $("#tab11").append(html);
                    
                    var foot = '<th class="text-center" colspan="2">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['s1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s4'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n4'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s5'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n5'])+'</th>';
                    $("#tab11").next("tfoot").empty();
                    $("#tab11").next("tfoot").append('<tr>'+foot+'</tr>');
                };
                break;
                
            case "12":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarJefaturaHogar/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarJefaturaHogar/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Jefatura</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });     
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab12_header").empty();
                    $("#tab12_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Jefatura del hogar Masculina",
                        "Jefatura del hogar Femenina"
                    );
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].jef_masculino , 't1');
                                    t2 = sumarValor(datos.tbody[j].jef_masculino_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].jef_masculino+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].jef_masculino_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].jef_femenina , 't1');
                                    t2 = sumarValor(datos.tbody[j].jef_femenina_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].jef_femenina+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].jef_femenina_por+"%</td>";
                                break;                                
                            }               
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'JEFATURA DEL HOGAR'};
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;

                        this.jef_masculino_por = this.jef_masculino_por.replace(",",".");
                        p1 = parseFloat(p1 + parseFloat(this.jef_masculino_por));                                    
                        this.jef_femenina_por = this.jef_femenina_por.replace(",",".");
                        p2 = parseFloat(p2 + parseFloat(this.jef_femenina_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Jefatura del hogar Masculina',
                                        y: p1
                                    },
                                    {
                                        name: 'Jefatura del hogar Femenina',
                                        y: p2
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc); 
                    
                    $("#tab12").empty();
                    $("#tab12").append(tbody);
                };
                break;
                
            case "13":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTipoHogares/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarTipoHogares/"+cod_ent;
                }
                
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Tipos</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });     
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab13_header").empty();
                    $("#tab13_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var variables = Array(
                        "Hogar Unipersonal",
                        "Hogar Nuclear",                    
                        "Hogar Extendido",
                        "Hogar Compuesto",
                        "Hogar Refugio",
                        "Hogar Colectividad"
                    );
            
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        $.each(datos.tbody,function(j,e){
                            iniciarValor();
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].unipersonal , 't1');
                                    t2 = sumarValor(datos.tbody[j].unipersonal_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].unipersonal+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].unipersonal_por+"%</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].nuclear , 't1');
                                    t2 = sumarValor(datos.tbody[j].nuclear_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].nuclear+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].nuclear_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].extendido , 't1');
                                    t2 = sumarValor(datos.tbody[j].extendido_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].extendido+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].extendido_por+"%</td>";
                                break;                                                                                              
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].compuesto , 't1');
                                    t2 = sumarValor(datos.tbody[j].compuesto_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].compuesto+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].compuesto_por+"%</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].refugio , 't1');
                                    t2 = sumarValor(datos.tbody[j].refugio_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].refugio+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].refugio_por+"%</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].colectividad , 't1');
                                    t2 = sumarValor(datos.tbody[j].colectividad_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].colectividad+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].colectividad_por+"%</td>";
                                break;                                                                                              
                            }         
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                            
                        });
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'TIPO DE HOGAR'};
                    $.each(datos.tbody,function(j,e){        
                        var p1 = 0;
                        var p2 = 0;
                        var p3 = 0;
                        var p4 = 0;
                        var p5 = 0;                    
                        var p6 = 0;                    
                        
                        this.unipersonal_por = this.unipersonal_por.replace(",",".");
                        p1 = parseFloat(p1 + parseFloat(this.unipersonal_por));                                    
                        this.nuclear_por = this.nuclear_por.replace(",",".");
                        p2 = parseFloat(p2 + parseFloat(this.nuclear_por));                        
                        this.extendido_por = this.extendido_por.replace(",",".");
                        p3 = parseFloat(p3 + parseFloat(this.extendido_por));                                    
                        this.compuesto_por = this.compuesto_por.replace(",",".");
                        p4 = parseFloat(p4 + parseFloat(this.compuesto_por));                        
                        this.refugio_por = this.refugio_por.replace(",",".");
                        p5 = parseFloat(p5 + parseFloat(this.refugio_por));                                    
                        this.colectividad_por = this.colectividad_por.replace(",",".");
                        p6 = parseFloat(p6 + parseFloat(this.colectividad_por));                        
                            
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {
                                        name: 'Hogar Unipersonal',
                                        y: p1
                                    },
                                    {
                                        name: 'Hogar Nuclear',
                                        y: p2
                                    },
                                    {
                                        name: 'Hogar Extendido',
                                        y: p3
                                    },
                                    {
                                        name: 'Hogar Compuesto',
                                        y: p4
                                    },
                                    {
                                        name: 'Hogar Refugio',
                                        y: p5
                                    },
                                    {
                                        name: 'Hogar Colectividad',
                                        y: p6
                                    }
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                    
                    
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc);
                    
                    $("#tab13").empty();
                    $("#tab13").append(tbody);
                };
                break;
                
            case "14":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarEquipamientoHogares/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarEquipamientoHogares/"+cod_ent;
                }
                iniciarValor();
                var exito = function(datos){   
                if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                var html = "";
                var s1 = 0; var n1 = 0;
                var s2 = 0; var n2 = 0;
                var s3 = 0; var n3 = 0;
                var s4 = 0; var n4 = 0;
                var s5 = 0; var n5 = 0;
                var s6 = 0; var n6 = 0;
                var s7 = 0; var n7 = 0;
                var s8 = 0; var n8 = 0;
                var s9 = 0; var n9 = 0;
                var s10 = 0; var n10 = 0;
                var s11 = 0; var n11 = 0
                var s12 = 0; var n12 = 0;
                var s13 = 0; var n13 = 0;
                var s14 = 0; var n14 = 0;
                var s15 = 0; var n15 = 0;

                var c = 1;
                $.each(datos,function(){
                    html += "<tr>";
                    html += "<td class='text-center' colspan='2'>"+this.cod_cir+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_vehiculo+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_vehiculo+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+this.con_telefonia_fija+"</td>";                    
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_celular+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_celular+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_tv_cable+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_tv_cable+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_computadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_computadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_internet+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_internet+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_radio+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_radio+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_tv+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_tv+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_nevera+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_nevera+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_lavadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_lavadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_secadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_secadora+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_calentador+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_calentador+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_aire_acondicionado+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_aire_acondicionado+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.con_cocina+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.sin_cocina+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_dispone+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_dispone+"</td>";
                    html += "</tr>";

                    this.con_vehiculo = this.con_vehiculo.replace(".","");
                    s1 = parseFloat(s1 + parseFloat(this.con_vehiculo));
                    this.sin_vehiculo = this.sin_vehiculo.replace(".","");
                    n1 = parseFloat(n1 + parseFloat(this.sin_vehiculo));
                    this.con_telefonia_fija = this.con_telefonia_fija.replace(".","");
                    s2 = parseFloat(s2 + parseFloat(this.con_telefonia_fija));
                    //this.con_telefonia_fija = this.con_telefonia_fija.replace(".","");
                    //n2 = parseFloat(n2 + parseFloat(this.no_acueducto));
                    this.con_celular = this.con_celular.replace(".","");
                    s3 = parseFloat(s3 + parseFloat(this.con_celular));
                    this.sin_celular = this.sin_celular.replace(".","");
                    n3 = parseFloat(n3 + parseFloat(this.sin_celular));
                    this.con_tv_cable = this.con_tv_cable.replace(".","");
                    s4 = parseFloat(s4 + parseFloat(this.con_tv_cable));
                    this.sin_tv_cable = this.sin_tv_cable.replace(".","");
                    n4 = parseFloat(n4 + parseFloat(this.sin_tv_cable));
                    this.con_computadora = this.con_computadora.replace(".","");
                    s5 = parseFloat(s5 + parseFloat(this.con_computadora));
                    this.sin_computadora = this.sin_computadora.replace(".","");
                    n5 = parseFloat(n5 + parseFloat(this.sin_computadora));
                    this.con_internet = this.con_internet.replace(".","");
                    s6 = parseFloat(s6 + parseFloat(this.con_internet));
                    this.sin_internet = this.sin_internet.replace(".","");
                    n6 = parseFloat(n6 + parseFloat(this.sin_internet));
                    this.con_radio = this.con_radio.replace(".","");
                    s7 = parseFloat(s7 + parseFloat(this.con_radio));
                    this.sin_radio = this.sin_radio.replace(".","");
                    n7 = parseFloat(n7 + parseFloat(this.sin_radio));
                    this.con_tv = this.con_tv.replace(".","");
                    s8 = parseFloat(s8 + parseFloat(this.con_tv));
                    this.sin_tv = this.sin_tv.replace(".","");
                    n8 = parseFloat(n8 + parseFloat(this.sin_tv));
                    this.con_nevera = this.con_nevera.replace(".","");
                    s9 = parseFloat(s9 + parseFloat(this.con_nevera));
                    this.sin_nevera = this.sin_nevera.replace(".","");
                    n9 = parseFloat(n9 + parseFloat(this.sin_nevera));
                    this.con_lavadora = this.con_lavadora.replace(".","");
                    s10 = parseFloat(s10 + parseFloat(this.con_lavadora));
                    this.sin_lavadora = this.sin_lavadora.replace(".","");
                    n10 = parseFloat(n10 + parseFloat(this.sin_lavadora));
                    this.con_secadora = this.con_secadora.replace(".","");
                    s11 = parseFloat(s11 + parseFloat(this.con_secadora));
                    this.sin_secadora = this.sin_secadora.replace(".","");
                    n11 = parseFloat(n11 + parseFloat(this.sin_secadora));
                    this.con_calentador = this.con_calentador.replace(".","");
                    s12 = parseFloat(s12 + parseFloat(this.con_calentador));
                    this.sin_calentador = this.sin_calentador.replace(".","");
                    n12 = parseFloat(n12 + parseFloat(this.sin_calentador));
                    this.con_aire_acondicionado = this.con_aire_acondicionado.replace(".","");
                    s13 = parseFloat(s13 + parseFloat(this.con_aire_acondicionado));
                    this.sin_aire_acondicionado = this.sin_aire_acondicionado.replace(".","");
                    n13 = parseFloat(n13 + parseFloat(this.sin_aire_acondicionado));
                    this.con_cocina = this.con_cocina.replace(".","");
                    s14 = parseFloat(s14 + parseFloat(this.con_cocina));
                    this.sin_cocina = this.sin_cocina.replace(".","");
                    n14 = parseFloat(n14 + parseFloat(this.sin_cocina));
                    this.no_dispone = this.no_dispone.replace(".","");
                    s15 = parseFloat(s15 + parseFloat(this.no_dispone));
                    this.si_dispone = this.si_dispone.replace(".","");
                    n15 = parseFloat(n15 + parseFloat(this.si_dispone));

                    agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);
                    
                    opc.title = {text: 'EQUIPAMIENTO DEL HOGAR'};
                    opc.tooltip = {};                            
                    opc.plotOptions = {};
                    opc.chart.type = 'column';
                    opc.xAxis = {
                            categories: [
                                'Hogares con Vehículo', 
                                'Hogares con Telefonía fija', 
                                'Teléfono celular', 
                                'Tv por cable', 
                                'Computadora',
                                'Internet', 
                                'Radio', 
                                'TV', 
                                'Nevera', 
                                'Lavadora',
                                'Secadora', 
                                'Calentador', 
                                'Aire Acondicionado', 
                                'Cocina', 
                                'Dispone de equipamiento'
                            ]
                        };
                    opc.series = [
                            {
                                name: 'Si',
                                data: [
                                    parseFloat(this.con_vehiculo),
                                    parseFloat(this.con_telefonia_fija),                    
                                    parseFloat(this.con_celular),
                                    parseFloat(this.con_tv_cable),
                                    parseFloat(this.con_computadora),
                                    parseFloat(this.con_internet),
                                    parseFloat(this.con_radio),
                                    parseFloat(this.con_tv),
                                    parseFloat(this.con_nevera),
                                    parseFloat(this.con_lavadora),
                                    parseFloat(this.con_secadora),
                                    parseFloat(this.con_calentador),
                                    parseFloat(this.con_aire_acondicionado),
                                    parseFloat(this.con_cocina),
                                    parseFloat(this.no_dispone)
                                ]
                            },
                            {
                                name: 'No',
                                data: [
                                    parseFloat(this.sin_vehiculo),
                                    '0',//parseFloat(this.con_telefonia_fija),                    
                                    parseFloat(this.sin_celular),
                                    parseFloat(this.sin_tv_cable),
                                    parseFloat(this.sin_computadora),
                                    parseFloat(this.sin_internet),
                                    parseFloat(this.sin_radio),
                                    parseFloat(this.sin_tv),
                                    parseFloat(this.sin_nevera),
                                    parseFloat(this.sin_lavadora),
                                    parseFloat(this.sin_secadora),
                                    parseFloat(this.sin_calentador),
                                    parseFloat(this.sin_aire_acondicionado),
                                    parseFloat(this.sin_cocina),
                                    parseFloat(this.si_dispone)
                                ]
                            }
                        ];

                        var selector = 'grafico_'+c;
                        cargarGrafico(selector , opc);
                        c++;
                    });

                    opc.series = [
                        {
                            name: 'Si',
                            data: [
                                s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15
                            ]
                        },
                        {
                            name: 'No',
                            data: [
                                n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15
                            ]
                        }
                    ];

                    cargarGrafico('grafico_0' , opc);
                    
                    $("#tab14").empty();
                    $("#tab14").append(html);
                    
                    
                    var foot = '<th class="text-center">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['s1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s4'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n4'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s5'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n5'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s6'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n6'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s7'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n7'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s8'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n8'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s9'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n9'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s10'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n10'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s11'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n11'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s12'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n12'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s13'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n13'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s14'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n14'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['s15'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n15'])+'</th>';
                    $("#tab14").next("tfoot").empty();
                    $("#tab14").next("tfoot").append('<tr>'+foot+'</tr>');
                };
                break;
                
            case "15":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarActividadEconomica/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarActividadEconomica/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                    var theader = "<th class='text-center'>Actividad</th>";
                    var c = 1;
                    var t1 = 0;
                    var t2 = 0;
                    $.each(datos.theader,function(){
                        theader += "<th class='text-center' class='text-center' colspan='2'>"+this.cod_cir+"</th>";
                        agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);                        
                        c++;
                    });     
                    theader += "<th class='text-center' class='text-center' colspan='2'>Totales</th>";
                    $("#tab15_header , #tab15A_header").empty();
                    $("#tab15_header , #tab15A_header").append("<tr>"+theader+"</tr>");
                    
                    var tbody = "";
                    var tbodyA = "";
                    var variablesA = Array(
                        "Activos",
                        "Inactivos",                    
                        "Tasa de Actividad Económica",
                        "Tasa de Ocupados",
                        "Tasa de Desocupados",
                        "Tasa de BTPPV"
                    );
                    
                    var variables = Array(
                        "Agricultura, ganadería, silvicultura y pesca",
                        "Extracción de carbón de piedra y lignito",
                        "Elaboración de productos alimenticios",
                        "Suministro de electricidad, gas, vapor y aire acondicionado",
                        "Captación, tratamiento y distribución de agua",
                        "Construcción de edificios",
                        "Comercio al por mayor y al por menor y reparación de vehículos automotores y motocicletas",
                        "Transporte por vía terrestre y transporte por tuberías",
                        "Actividades de alojamiento",
                        "Actividades de edición",
                        "Actividades de servicios financieros, excepto las de seguros y fondos de pensiones",
                        "Actividades inmobiliarias",
                        "Actividades jurídicas y de contabilidad",
                        "Actividades de alquiler y arrendamiento",
                        "Administración pública y la defensa; planes de seguridad social de afiliación obligatoria",
                        "Enseñanza",
                        "Actividades relacionadas con la salud humana",
                        "Actividades creativas, artísticas y de entretenimiento",
                        "Actividades de asociaciones",
                        "Actividades de los hogares como empleadores de personal doméstico",
                        "Actividades de organizaciones y órganos extraterritoriales",
                        "Actividades no bien especificadas",
                        "Omitidas"
                    );
            
                    var data = new Array();
                    for(var i = 0; i < variablesA.length; i++)
                    {
                        tbodyA += "<tr>";
                        tbodyA += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        iniciarValor();
                        $.each(datos.tbody,function(j,e){
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].inactivo , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].inactivo+"</td>";
                                break;
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].activos , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].activos+"</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].tasa_act_economica , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].tasa_act_economica+"</td>";
                                break;                                                                                              
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].tasa_ocupados , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].tasa_ocupados+"</td>";
                                break;
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].tasa_desocupados , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].tasa_desocupados+"</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].tasa_btppv , 't1');
                                    tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='2'>"+datos.tbody[j].tasa_btppv+"</td>";
                                break;                
                            }       
                        });                        
                        tbodyA += "<td class='text-center' style='vertical-align: middle;' colspan='3'>"+formatValor(t1)+"</td>";
                        tbodyA += "</tr>";
                    }
                    
                    $("#tab15A").empty();
                    $("#tab15A").append(tbodyA);
                    
                    var t1 = 0;
                    var t2 = 0;
                    var data = new Array();
                    for(var i = 0; i < variables.length; i++)
                    {
                        tbody += "<tr>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+variables[i]+"</td>";
                        
                        iniciarValor();
                        $.each(datos.tbody,function(j,e){                            
                            switch (i)
                            {
                                case 0:
                                    t1 = sumarValor(datos.tbody[j].agricultura , 't1');
                                    t2 = sumarValor(datos.tbody[j].agricultura_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].agricultura+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].agricultura_por+"%</td>";
                                break;                                
                                case 1:
                                    t1 = sumarValor(datos.tbody[j].extraccion , 't1');
                                    t2 = sumarValor(datos.tbody[j].extraccion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].extraccion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].extraccion_por+"%</td>";
                                break;                                
                                case 2:
                                    t1 = sumarValor(datos.tbody[j].elaboracion , 't1');
                                    t2 = sumarValor(datos.tbody[j].elaboracion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].elaboracion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].elaboracion_por+"%</td>";
                                break;                                
                                case 3:
                                    t1 = sumarValor(datos.tbody[j].suministro , 't1');
                                    t2 = sumarValor(datos.tbody[j].suministro_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].suministro+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].suministro_por+"%</td>";
                                break;                                
                                case 4:
                                    t1 = sumarValor(datos.tbody[j].captacion , 't1');
                                    t2 = sumarValor(datos.tbody[j].captacion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].captacion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].captacion_por+"%</td>";
                                break;                                
                                case 5:
                                    t1 = sumarValor(datos.tbody[j].construccion , 't1');
                                    t2 = sumarValor(datos.tbody[j].construccion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].construccion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].construccion_por+"%</td>";
                                break;                                
                                case 6:
                                    t1 = sumarValor(datos.tbody[j].comercio , 't1');
                                    t2 = sumarValor(datos.tbody[j].comercio_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].comercio+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].comercio_por+"%</td>";
                                break;                                
                                case 7:
                                    t1 = sumarValor(datos.tbody[j].transporte , 't1');
                                    t2 = sumarValor(datos.tbody[j].transporte_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].transporte+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].transporte_por+"%</td>";
                                break;                                
                                case 8:
                                    t1 = sumarValor(datos.tbody[j].actividades_alojamiento , 't1');
                                    t2 = sumarValor(datos.tbody[j].actividades_alojamiento_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].actividades_alojamiento+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].actividades_alojamiento_por+"%</td>";
                                break;                                
                                case 9:
                                    t1 = sumarValor(datos.tbody[j].actividades_edicion , 't1');
                                    t2 = sumarValor(datos.tbody[j].actividades_edicion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].actividades_edicion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].actividades_edicion_por+"%</td>";
                                break;                                
                                case 10:
                                    t1 = sumarValor(datos.tbody[j].financieras , 't1');
                                    t2 = sumarValor(datos.tbody[j].financieras_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].financieras+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].financieras_por+"%</td>";
                                break;                                
                                case 11:
                                    t1 = sumarValor(datos.tbody[j].inmobiliarias , 't1');
                                    t2 = sumarValor(datos.tbody[j].inmobiliarias_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].inmobiliarias+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].inmobiliarias_por+"%</td>";
                                break;                                
                                case 12:
                                    t1 = sumarValor(datos.tbody[j].juridicas , 't1');
                                    t2 = sumarValor(datos.tbody[j].juridicas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].juridicas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].juridicas_por+"%</td>";
                                break;                                
                                case 13:
                                    t1 = sumarValor(datos.tbody[j].alquiler , 't1');
                                    t2 = sumarValor(datos.tbody[j].alquiler_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].alquiler+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].alquiler_por+"%</td>";
                                break;                                
                                case 14:
                                    t1 = sumarValor(datos.tbody[j].administracion , 't1');
                                    t2 = sumarValor(datos.tbody[j].administracion_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].administracion+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].administracion_por+"%</td>";
                                break;                                
                                case 15:
                                    t1 = sumarValor(datos.tbody[j].ensenanza , 't1');
                                    t2 = sumarValor(datos.tbody[j].ensenanza_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].ensenanza+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].ensenanza_por+"%</td>";
                                break;                                
                                case 16:
                                    t1 = sumarValor(datos.tbody[j].salud , 't1');
                                    t2 = sumarValor(datos.tbody[j].salud_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].salud+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].salud_por+"%</td>";
                                break;                                
                                case 17:
                                    t1 = sumarValor(datos.tbody[j].creativas , 't1');
                                    t2 = sumarValor(datos.tbody[j].creativas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].creativas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].creativas_por+"%</td>";
                                break;                                
                                case 18:
                                    t1 = sumarValor(datos.tbody[j].asociaiones , 't1');
                                    t2 = sumarValor(datos.tbody[j].asociaiones_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].asociaiones+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].asociaiones_por+"%</td>";
                                break;                                
                                case 19:
                                    t1 = sumarValor(datos.tbody[j].hogares , 't1');
                                    t2 = sumarValor(datos.tbody[j].hogares_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hogares+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].hogares_por+"%</td>";
                                break;                                
                                case 20:
                                    t1 = sumarValor(datos.tbody[j].organizaciones , 't1');
                                    t2 = sumarValor(datos.tbody[j].organizaciones_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].organizaciones+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].organizaciones_por+"%</td>";
                                break;                                
                                case 21:
                                    t1 = sumarValor(datos.tbody[j].no_especifica , 't1');
                                    t2 = sumarValor(datos.tbody[j].no_especifica_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].no_especifica+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].no_especifica_por+"%</td>";
                                break;                                
                                case 22:
                                    t1 = sumarValor(datos.tbody[j].omitidas , 't1');
                                    t2 = sumarValor(datos.tbody[j].omitidas_por , 't2');
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].omitidas+"</td>";
                                    tbody += "<td class='text-center' style='vertical-align: middle;'>"+datos.tbody[j].omitidas_por+"%</td>";
                                break;                                
                            }       
                            
                            data[i] = {
                                name:variables[i],
                                y:t2
                            };
                        });
                        
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+formatValor(t1)+"</td>";
                        tbody += "<td class='text-center' style='vertical-align: middle;'>"+t2.toFixed(2).replace('.',',')+"%</td>";
                        tbody += "</tr>";
                    }
                    
                    opc.title = {text: 'OCUPACION Y RAMA DE ACTIVIDAD ECONOMICA'};
                    var p1 = 0;var p2 = 0;var p3 = 0;var p4 = 0;var p5 = 0;
                    var p6 = 0;var p7 = 0;var p8 = 0;var p9 = 0;var p10 = 0;
                    var p11 = 0;var p12 = 0;var p13 = 0;var p14 = 0;var p15 = 0;
                    var p16 = 0;var p17 = 0;var p18 = 0;var p19 = 0;var p20 = 0;
                    var p21 = 0;var p22 = 0;var p23 = 0;
                    
                    
                    $.each(datos.tbody,function(j,e){                                
                        iniciarValor();
                        this.agricultura_por = this.agricultura_por.replace(',','.');
                        p1 = sumarValor(this.agricultura_por , 'p1');
                        this.extraccion_por = this.extraccion_por.replace(',','.');
                        p2 = sumarValor(this.extraccion_por , 'p2');
                        this.elaboracion_por = this.elaboracion_por.replace(',','.');
                        p3 = sumarValor(this.elaboracion_por , 'p3');
                        this.suministro_por = this.suministro_por.replace(',','.');
                        p4 = sumarValor(this.suministro_por , 'p4');
                        this.captacion_por = this.captacion_por.replace(',','.');
                        p5 = sumarValor(this.captacion_por , 'p5');
                        this.construccion_por = this.construccion_por.replace(',','.');
                        p6 = sumarValor(this.construccion_por , 'p6');
                        this.comercio_por = this.comercio_por.replace(',','.');
                        p7 = sumarValor(this.comercio_por , 'p7');
                        this.transporte_por = this.transporte_por.replace(',','.');
                        p8 = sumarValor(this.transporte_por , 'p8');
                        this.actividades_alojamiento_por = this.actividades_alojamiento_por.replace(',','.');
                        p9 = sumarValor(this.actividades_alojamiento_por , 'p9');
                        this.actividades_edicion_por = this.actividades_edicion_por.replace(',','.');
                        p10 = sumarValor(this.actividades_edicion_por , 'p10');
                        this.financieras_por = this.financieras_por.replace(',','.');
                        p11 = sumarValor(this.financieras_por , 'p11');
                        this.inmobiliarias_por = this.inmobiliarias_por.replace(',','.');
                        p12 = sumarValor(this.inmobiliarias_por , 'p12');
                        this.juridicas_por = this.juridicas_por.replace(',','.');
                        p13 = sumarValor(this.juridicas_por , 'p13');
                        this.alquiler_por = this.alquiler_por.replace(',','.');
                        p14 = sumarValor(this.alquiler_por , 'p14');
                        this.administracion_por = this.administracion_por.replace(',','.');
                        p15 = sumarValor(this.administracion_por , 'p15');
                        this.ensenanza_por = this.ensenanza_por.replace(',','.');
                        p16 = sumarValor(this.ensenanza_por , 'p16');
                        this.salud_por = this.salud_por.replace(',','.');
                        p17 = sumarValor(this.salud_por , 'p17');
                        this.creativas_por = this.creativas_por.replace(',','.');
                        p18 = sumarValor(this.creativas_por , 'p18');
                        this.asociaiones_por = this.asociaiones_por.replace(',','.');
                        p19 = sumarValor(this.asociaiones_por , 'p19');
                        this.hogares_por = this.hogares_por.replace(',','.');
                        p20 = sumarValor(this.hogares_por , 't20');
                        this.organizaciones_por = this.organizaciones_por.replace(',','.');
                        p21 = sumarValor(this.organizaciones_por , 'p21');
                        this.no_especifica_por = this.no_especifica_por.replace(',','.');
                        p22 = sumarValor(this.no_especifica_por , 'p22');
                        this.omitidas_por = this.omitidas_por.replace(',','.');
                        p23 = sumarValor(this.omitidas_por , 'p23');
                           
                        opc.series = [
                            {
                                type: 'pie',
                                colorByPoint: true,
                                data: [
                                    {name: 'Agricultura, ganadería, silvicultura y pesca',y: p1},
                                    {name: 'Extracción de carbón de piedra y lignito',y: p2},
                                    {name: 'Elaboración de productos alimenticios',y: p3},
                                    {name: 'Suministro de electricidad, gas, vapor y aire acondicionado',y: p4},
                                    {name: 'Captación, tratamiento y distribución de agua',y: p5},
                                    {name: 'Construcción de edificios',y: p6},
                                    {name: 'Comercio al por mayor y al por menor y reparación de vehículos automotores y motocicletas',y: p7},
                                    {name: 'Transporte por vía terrestre y transporte por tuberías',y: p8},
                                    {name: 'Actividades de alojamiento',y: p9},
                                    {name: 'Actividades de edición',y: p10},
                                    {name: 'Actividades de servicios financieros, excepto las de seguros y fondos de pensiones',y: p11},
                                    {name: 'Actividades inmobiliarias',y: p12},
                                    {name: 'Actividades jurídicas y de contabilidad',y: p13},
                                    {name: 'Actividades de alquiler y arrendamiento',y: p14},
                                    {name: 'Administración pública y la defensa',y: p15},
                                    {name: 'Enseñanza',y: p16},
                                    {name: 'Actividades relacionadas con la salud humana',y: p17},
                                    {name: 'Actividades creativas, artísticas y de entretenimiento',y: p18},
                                    {name: 'Actividades de asociaciones',y: p19},
                                    {name: 'Actividades de los hogares como empleadores de personal doméstico',y: p20},
                                    {name: 'Actividades de organizaciones y órganos extraterritoriales',y: p21},
                                    {name: 'Actividades no bien especificadas',y: p22},
                                    {name: 'Omitidas',y: p23}
                                ]
                            }
                        ];
                            
                        var selector = 'grafico_'+(parseInt(j)+1);
                        cargarGrafico(selector , opc); 
                    });
                                        
                    opc.series = [
                        {
                            type: 'pie',
                            colorByPoint: true,
                            data: data
                        }
                    ];
                    cargarGrafico('grafico_0' , opc);
                    
                    $("#tab15").empty();
                    $("#tab15").append(tbody);
                };
                break;
                
            case "16":
                if(id_cir != 0)
                {
                    var url = base_url+"circunscripciones/ajaxConsultarAsistenciaEscolar/"+cod_ent+"/"+id_cir;
                }
                else
                {
                    var url = base_url+"circunscripciones/ajaxConsultarAsistenciaEscolar/"+cod_ent;
                }
                var exito = function(datos){   
                    if(id_cir == 0){agregarGrafico(0 , "General",true);}                    
                var html = "";
                var s1 = 0; var n1 = 0; var p1 = 0;
                var s2 = 0; var n2 = 0; var p2 = 0;
                var s3 = 0; var n3 = 0; var p3 = 0;
                var s4 = 0; var n4 = 0; var p4 = 0;
                var c = 1;
                iniciarValor();
                $.each(datos,function(){
                    html += "<tr>";
                    html += "<td class='text-center' colspan='2'>"+this.cod_cir+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_ini+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_asi_esc_ini+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_ini_por+"%</td>";                    
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_pri+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_asi_esc_pri+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_pri_por+"%</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_sec+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_asi_esc_sec+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_sec_por+"%</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_sup+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.no_asi_esc_sup+"</td>";
                    html += "<td class='text-center' style='vertical-align: middle;'>"+this.si_asi_esc_sup_por+"%</td>";
                    //html += "<td class='text-center' style='vertical-align: middle;'>"+this.pro_ano_est+"</td>";
                    html += "</tr>";
                    
                    this.si_asi_esc_ini_por = this.si_asi_esc_ini_por.replace(",",".");
                    this.si_asi_esc_pri_por = this.si_asi_esc_pri_por.replace(",",".");
                    this.si_asi_esc_sec_por = this.si_asi_esc_sec_por.replace(",",".");
                    this.si_asi_esc_sup_por = this.si_asi_esc_sup_por.replace(",",".");
                    
                    p1 = (p1 + parseFloat(this.si_asi_esc_ini_por));
                    p2 = (p2 + parseFloat(this.si_asi_esc_pri_por));
                    p3 = (p3 + parseFloat(this.si_asi_esc_sec_por));
                    p4 = (p4 + parseFloat(this.si_asi_esc_sup_por));
                    
                    this.si_asi_esc_ini = this.si_asi_esc_ini.replace(".","");
                    s1 = sumarValor(this.si_asi_esc_ini , 's1');//parseFloat(s1 + parseFloat(this.si_asi_esc_ini));
                    this.no_asi_esc_ini = this.no_asi_esc_ini.replace(".","");
                    n1 = sumarValor(this.no_asi_esc_ini , 'n1');//parseFloat(n1 + parseFloat(this.no_asi_esc_ini));
                    this.si_asi_esc_pri = this.si_asi_esc_pri.replace(".","");
                    s2 = sumarValor(this.si_asi_esc_pri , 's2');//parseFloat(s2 + parseFloat(this.si_asi_esc_pri));
                    this.no_asi_esc_pri = this.no_asi_esc_pri.replace(".","");
                    n2 = sumarValor(this.no_asi_esc_pri , 'n2');//parseFloat(n2 + parseFloat(this.no_asi_esc_pri));
                    this.si_asi_esc_sec = this.si_asi_esc_sec.replace(".","");
                    s3 = sumarValor(this.si_asi_esc_sec , 's3');//parseFloat(s3 + parseFloat(this.si_asi_esc_sec));
                    this.no_asi_esc_sec = this.no_asi_esc_sec.replace(".","");
                    n3 = sumarValor(this.no_asi_esc_sec , 'n3');//parseFloat(n3 + parseFloat(this.no_asi_esc_sec));
                    this.si_asi_esc_sup = this.si_asi_esc_sup.replace(".","");
                    s4 = sumarValor(this.si_asi_esc_sup , 's4');//parseFloat(s4 + parseFloat(this.si_asi_esc_sup));
                    this.no_asi_esc_sup = this.no_asi_esc_sup.replace(".","");
                    n4 = sumarValor(this.no_asi_esc_sup , 'n4');//parseFloat(n4 + parseFloat(this.no_asi_esc_sup));

                    agregarGrafico(c , this.cod_cir,(id_cir != 0)?true:false);
                    opc.title = {text: 'ASISTENCIA ESCOLAR'};
                    opc.tooltip = {};                            
                    opc.plotOptions = {};
                    opc.chart.type = 'column';
                    opc.xAxis = {
                            categories: [
                                "Asistencia escolar inicial (3 a 6 años)",
                                "No asiste educación inicial (3 a 6 años)",                    
                                "Asistencia escolar primaria (7 a 12 años)",
                                "No asiste educación primaria (7 a 12 años)",
                                "Asistencia escolar secundaria (13 a 17 años)",
                                "No asiste educación secundaria (13 a 17 años)",
                                "Asistencia escolar superior (18 a 24 años)",
                                "No asiste educación superior (18 a 24 años)",
                                "No asiste educación superior (18 a 24 años)",
                            ]
                        };
                    opc.series = [
                            {
                                name: 'Si',
                                data: [
                                    parseFloat(this.si_asi_esc_ini),
                                    parseFloat(this.si_asi_esc_pri),                    
                                    parseFloat(this.si_asi_esc_sec),
                                    parseFloat(this.si_asi_esc_sup)
                                ]
                            },
                            {
                                name: 'No',
                                data: [
                                    parseFloat(this.no_asi_esc_ini),
                                    parseFloat(this.no_asi_esc_pri),
                                    parseFloat(this.no_asi_esc_sec),
                                    parseFloat(this.no_asi_esc_sup)
                                ]
                            }
                        ];

                        var selector = 'grafico_'+c;
                        cargarGrafico(selector , opc);
                        c++;
                    });

                    opc.series = [
                        {
                            name: 'Si',
                            data: [
                                s1,s2,s3,s4
                            ]
                        },
                        {
                            name: 'No',
                            data: [
                                n1,n2,n3,n4
                            ]
                        }
                    ];

                    cargarGrafico('grafico_0' , opc);
                    
                    $("#tab16").empty();
                    $("#tab16").append(html);
                    
                    var foot = '<th class="text-center" colspan="2">Totales</th>';
                    foot += '<th class="text-center">'+formatValor(total['s1'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n1'])+'</th>';
                    foot += '<th class="text-center">'+p1.toFixed(2).replace('.',',')+'%</th>';
                    foot += '<th class="text-center">'+formatValor(total['s2'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n2'])+'</th>';
                    foot += '<th class="text-center">'+p2.toFixed(2).replace('.',',')+'%</th>';
                    foot += '<th class="text-center">'+formatValor(total['s3'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n3'])+'</th>';
                    foot += '<th class="text-center">'+p3.toFixed(2).replace('.',',')+'%</th>';
                    foot += '<th class="text-center">'+formatValor(total['s4'])+'</th>';
                    foot += '<th class="text-center">'+formatValor(total['n4'])+'</th>';
                    foot += '<th class="text-center">'+p4.toFixed(2).replace('.',',')+'%</th>';
                    $("#tab16").next("tfoot").empty();
                    $("#tab16").next("tfoot").append('<tr>'+foot+'</tr>');
                    
                };
                break;
                
            default :
                break;
        }
    }
    else
    {
        $(".tabbable").addClass("hidden");
        return false;
    }
    $(".tabla"+tip_dat).removeClass("hidden");
    peticionAjax(url , {}, "POST" , "json" , exito);
}

function cargarGrafico(selector , opc)
{
    $("#"+selector).highcharts(opc);
}   

function agregarGrafico(index , titulo ,activo)
{
    var act = "";
    if(activo)
    {
        act = "active";
    }
    
    $(".tabbable").removeClass("hidden");
    $(".tabbable").find(".nav-tabs").append('<li class="'+act+'"><a href="#'+index+'" data-toggle="tab">'+titulo+'</a></li>');
    $(".tabbable").find(".tab-content").append('<div class="tab-pane '+act+'" id="'+index+'"><div id="grafico_'+index+'"></div></div>');
}   