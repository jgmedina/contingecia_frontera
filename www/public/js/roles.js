function habilitar(obj , id , id_per)
{
    $obj = $(obj);
    $.ajax({
        url: base_url+"roles/ajaxHabilitar/"+id+"/"+id_per,
        dataType: "json",            
        success:function(data){
            $obj.find("span").attr("title","Deshabilitar");             
            $obj.find("span").attr("class","glyphicon glyphicon-ok");
            $obj.attr("onclick","deshabilitar(this , '"+id+"','"+id_per+"')");
        }
    });
}

function deshabilitar(obj , id , id_per)
{
    $obj = $(obj);
    $.ajax({
        url: base_url+"roles/ajaxDeshabilitar/"+id+"/"+id_per,
        dataType: "json",            
        success:function(data){
            $obj.find("span").attr("title","Habilitar");             
            $obj.find("span").attr("class","glyphicon glyphicon-remove");
            $obj.attr("onclick","habilitar(this , '"+id+"','"+id_per+"')");
        }
    });
}

function moverDesdeHasta(origen , destino)
{
    var $destino = $("#"+destino);
    var $origen = $("#"+origen+" option:selected");
    $destino.append($origen);
}

function actualizarPagIni()
{
    var $obj = $("#pan_ini option");
    var c = 0;
    $.each($obj,function(i,e){
        var texto = $(this).text();
        if(texto.search(/\([0-9]\)/) > 0)
        {
            texto = texto.replace(/\([0-9]\)/, "("+ ++c +")");            
        }
        else
        {
            texto += "("+ ++c +")";
        }
        $(this).text(texto);
    });
    
}

function actualizarItems()
{
    var $obj = $("#items option");
    $.each($obj,function(i,e){
        var texto = $(this).text().replace(/\([0-9]\)/, "");
        $(this).text(texto);
    });
}

function seleccionarTodo()
{
    $.each($("#pan_ini option"),function(){
        $(this).attr("selected","selected");
    });
}