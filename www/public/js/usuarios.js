function refrescarRoles(selector)
{
    $.ajax({
        url: base_url+"roles/ajaxListarRoles",
        type: "POST",            
        dataType: "json",            
        success:function(data){
            $("#"+selector).empty();
            $.each(data,function(i,e){
                $("#"+selector).append('<option value="'+e.id+'">'+e.nom_rol+'</option>');
            });            
        }
    });
}
/*
function buscarPersonal(obj)
{
    var $personal = $(obj).parents('.row');
    var val = $("#ci_per").val();
    if(val.length > 3)
    {
        $.ajax({
            url: base_url+"personal/ajaxConsultarPersonalPorCedula/"+val,
            type: "GET",            
            dataType: "json",            
            success:function(respuesta){
                if(respuesta.total > 0)
                {
                    $personal.find("#list_per").attr("size",respuesta.total);
                    $personal.find("#list_per").removeClass("hidden");
                    $personal.find("#list_per").empty();
                    $.each(respuesta.data,function(i,e){
                        $personal.find("#list_per").append('<option value="'+e.id_per+'" src-foto="'+e.fot_per+'">'+e.pri_nom_per+' '+e.pri_ape_per+' </option>');
                    });   
                }
                else
                {
                    $personal.find("#list_per").attr("size",respuesta.total);
                    $personal.find("#list_per").addClass("hidden");
                }
            }
        });
    }
    else
    {
        $("#list_per").addClass("hidden");
    }
}

function seleccionarPersonal(obj)
{
    var $personal = $(obj).parents('.row');
    $personal.find("#nom_ape_per , #id_per_trip").val('');
    var $val = $(obj).find('option:selected');
    var id = $val.val();
    var foto = $val.attr("src-foto");
    var nom_ape = $val.text();
    var src;
    if(foto != "null")
    {
        if($('.img-thumbnail').attr('src').split("/").length >10)
        {
            src = $('.img-thumbnail').attr('src').replace($('.img-thumbnail').attr('src').split("/").pop(-1),foto);
        }
        else
        {
            src = $('.img-thumbnail').attr('src').replace($('.img-thumbnail').attr('src').split("/").pop(-1),"personal/"+foto);
        }
    }
    else
    {
        if($('.img-thumbnail').attr('src').split("/").length >10)
        {
            var tmp = $('.img-thumbnail').attr('src').replace("personal/","");
            src = tmp.replace(tmp.split("/").pop(-1),"user-default.jpg");
        }
        else
        {
            src = $('.img-thumbnail').attr('src').replace($('.img-thumbnail').attr('src').split("/").pop(-1),"user-default.jpg");
        }
        
    }
    $('.img-thumbnail').attr('src', src);
    
    $personal.find("#nom_ape_per").val(nom_ape);
    $personal.find("#id_per_trip").val(id);
    $personal.find("#list_per").addClass("hidden");
}
*/