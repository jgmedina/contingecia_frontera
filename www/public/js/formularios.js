function cambiarFormulario(elemento,controlador,metodo)
{
    var text = Array("required" , "minlength" , "maxlength" , "regex");
    var tel = Array("required" , "minlength" , "maxlength" , "regex");
    var email = Array("required" , "minlength" , "maxlength" , "regex");
    var number = Array("required" , "min" , "max");
    var select = Array("required" , "min");
    var textarea = Array("required" , "minlength" , "maxlength" , "regex");
    var date = Array("required" , "minlength" , "maxlength" , "regex");
    var password = Array("required" , "minlength" , "maxlength" , "regex");
    var checkbox = Array("required" , "maxchecked" , "minchecked");
    
    var $elemento = $(elemento);
    var id_cam = $elemento.val();
    var tipo = $elemento.find('option:selected').attr("tipo");
    var tipos_val = eval($elemento.find('option:selected').attr("tipo"));
    $("form")[0].reset();
    $("#id_cam").val(id_cam);
    $.each($(".contenedor").find(".row") , function(){
        var $row = $(this);
        $row.addClass("hidden");
        
        $.each(tipos_val , function(i,e){
            if(typeof($row.find("#"+e)[0]) != "undefined")
            {
                $row.removeClass("hidden");
            }
        });
        
    });
    
    $.ajax({
        url: base_url+"formularios/ajaxConsultarCampo/"+id_cam+"/"+controlador+"/"+metodo,
        type: "POST",            
        dataType: "json",            
        success:function(data){
            $.each(data,function(){
                if(this.tip != null)
                {
                    if( this.tip == "data-validation-regex-regex")
                    {
                        $("#regex").val(this.val);
                        $("#regex_msg").val(this.msg);
                    }
                    else
                    {
                        if( this.tip == "required")
                        {
                            if(this.val == "1")
                            {
                                $("#"+this.tip).attr('checked', true);
                            }
                            else
                            {
                                $("#"+this.tip).attr('checked', false);
                            }
                        }
                        $("#"+this.tip).val(this.val);
                        $("#"+this.tip+"_msg").val(this.msg);
                    }
                }
            });            
        }
    });
}
