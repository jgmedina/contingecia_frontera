function agregarGrupo(obj)
{
    var $obj = $(obj);
    var visible = $obj.attr("visible");
    
    if(JSON.parse(visible))
    {
        $(".select_grupo").addClass("hidden");
        $(".gru_uni_med").removeClass("hidden");
        $obj.attr("visible",false);
    }
    else
    {
        $(".select_grupo").removeClass("hidden");
        $(".gru_uni_med").addClass("hidden");
        $obj.attr("visible",true);
    }
}

function seleccionarGrupo(obj)
{
    console.log("seleccionarGrupo");
    
    var $obj = $(obj);
    if($obj.prop("tagName") == "SELECT")
    {
        var valor = $obj.find("option:selected").val();
        if(valor != 0)
        {
            $("#gru_uni_med").val(valor);
        }
        else
        {
            $("#gru_uni_med").val('');
        }
    }
    else
    {
        $("#gru_uni_med").val($obj.val());
    }
}

