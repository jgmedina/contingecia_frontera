var base_url = window.location.href.match(/[/A-Za-z0-9\:\_\.]*www\//);[0];
//var base_url = window.location.href.match(/[/A-Za-z0-9\:\_\.]*.ve\//);[0];

var pro_pri = 1;
var sec_eco = 1;

var dataTables = {
            "oLanguage": {
                    "oPaginate": {
                                    "sFirst":"Primero ", 
                                    "sPrevious":"Anterior ", 
                                    "sNext":"Siguiente ", 
                                    "sLast":"Último "
                                },
                    "sLengthMenu": "_MENU_ ",
                    "sZeroRecords": "No se encontro data",
                    "sInfo": "_START_ - _END_ / _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 - 0 / 0 registros",
                    "sInfoFiltered": "(Filtrado de _MAX_ registros en total)",
                    "sSearch": "",
                    "sLengthMenu": '<select>'+
                                        '<option value="5" selected>5 Registros</option>'+
                                        '<option value="10">10 Registros</option>'+
                                        '<option value="20">20 Registros</option>'+        
                                        '<option value="-1">Todos</option>'+
                                    '</select> ',                                                                    
                    "sLoadingRecords": "Por favor espere - Cargando..."
            },
            "sDom": '<"top"lf>rt<"bottom"><".col-md-8"p><"clear">',
            "bStateSave": true,
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "Todos"]],
            "aoColumnDefs": [
                                { 
                                    "bSortable": false, 
                                    "aTargets": [ 4 ] 
                                }
                                ],
            "fnInitComplete": function() { 
                $("input , select").addClass("form-control");
                $("input").attr("placeholder", "Filtrar");
            }
    };
    
var bootboxOpc = {
        animate: true,
        show: true,
        keyboard : true,
        backdrop: true,
        closeButton: true,
        buttons: {
            success: {   
            label: "Ok",
            className: "btn-success"
            }
        }
    };
    
$(document).ready(function() {     
    
    /*
    $.each($(".panel-body"),function(){
        $(this).find('.form-control,.control-group').last().focusout(function(){
            var id = "#paso"+(parseInt($(this).parents(".panel-collapse").attr("id").replace("paso", "")) + 1);
            $(this).parents(".panel-collapse").collapse('hide');
            $(id).collapse('show');
            $(id).find(".form-control").first().focus()
        });
    });
    */
    
    $.each($("input[type='text'],textarea"),function(i,e){
       $(e).attr("placeholder",$(e).parent().parent().find('label').text());
    });
    
    $.each($("input[type='number']"),function(i,e){
       $(e).val(0);
    });
    
    $("#hom_adm_pla , #muj_adm_pla , #hom_pro_pla , #muj_pro_pla").change(function(){
        var total = parseInt(0);
        $.each($("#hom_adm_pla , #muj_adm_pla , #hom_pro_pla , #muj_pro_pla"),function(){
           total = total + parseInt($(this).val());
        });
        $("#tot_pla").val(total);
    });
    
    activarDatepicker();

    $(".navbar-default h4, .navbar-default h6").css({"color":$(".navbar-default .navbar-nav > li > a ").css("color")});

    if($('#map').length > 0)
    {
        init();
    }

    $.each($('a[href*="action"]'),function(i , e){
        if($(e).length > 0)
        {
            //$(this).addClass("disabled");
        }
    });

    $.ajax({
        url: base_url + "acl/ajaxPermisos",
        type: "POST",            
        dataType: "json",
        data:{"ajax":true},
        success: function(data){
            $.each(data.perms,function(index){
                var controlador = index.split("_")[0];
                var metodo = "action"+index.split("_")[1][0].toUpperCase()+index.split("_")[1].slice(1,index.split("_")[1].length);
                $.each($("a[href*='"+controlador+"/"+metodo+"']"),function(){
                    $(this).removeClass("disabled");
                });
            });
        }
    });
});    

function ejecutarModal(html,title,btnSuccess,btnDanger,BtnMain)
{                
    bootboxOpc.title = (typeof(title) != "undefined")?title:"";
    var buttons = {};
    ((typeof(btnSuccess) != "undefined")?buttons.success = btnSuccess:{});
    ((typeof(btnDanger) != "undefined")?buttons.danger = btnDanger:{});
    ((typeof(BtnMain) != "undefined")?buttons.main = BtnMain:{});
    bootboxOpc.buttons = buttons;
    bootboxOpc.message = html
    bootbox.dialog(bootboxOpc);

}

function cargarModal(controlador,metodo,callback)
{                
    ((typeof(callback) != "undefined")?callback = callback:callback = function(){});
    $.ajax({
        url: base_url+controlador+"/"+metodo,
        type: "POST",            
        dataType: "html",            
        success:function(html){      
            $html = $(html).find('form');
            var $boton = $($html.find('button[type="submit"]')[0]);                                
            var action = $html.attr('action').replace(/action/,'ajax');
            $html.attr('action',action);                
            $.each($html.find('a,input,button'),function(i,elemento){
                if($(elemento).attr("role") == "button")
                {
                    $html.find($(elemento)).remove();
                }
            });                          

            buttons = {   
                label: $boton.html(),
                className: $boton.attr("class"),
                callback: function(){
                        var data = $('form[action="'+action+'"]').find('input,textarea').serializeArray();
                        $.post(action,data, function(resultado) {                                                                                                
                            var respuesta = $(resultado).find('#msgModal').html();                                
                            eval(callback);
                            eval(respuesta);                                 
                       });
                    }
            };

            html = "<form role='form' action='"+action+"' method='POST'>"+$html.html()+"</form>"
            ejecutarModal(html,"",buttons);
        }
    });

}

function expandirInformacion(titulo,html,callback)
{
    ((typeof(callback) != "undefined")?callback = callback:callback = function(){});

    $('#modal').modal('show');        
    $('#modal').on('shown.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-title').text(titulo);
        modal.find('.modal-body').empty();
        modal.find('.modal-body').append(html);
        eval(callback);
    });
}    

function cargarUsuario(selector)
{
    var text = $("#"+selector).text().trim();
    var html = $("#"+selector).html();
    //html = html.replace( text , ape_usu+" "+nom_usu);
    $("#"+selector).html(html);
}

function cargando()
{
    var html = '<div class="progress progress-striped active">';
    html +='<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">';
    html +='<span class="sr-only">45% completado</span></div></div>';
    expandirInformacion("Cargando...",html);                
    var c = 0;
    var progress = setInterval(function() {
        var $bar = $('.progress-bar');

        if (c > 100) 
        {
            clearInterval(progress);
            $('.progress').removeClass('active');
            $('#modal').modal('hide');        
        } 
        else 
        {
            c = c+10;
            $bar.width(c+"%");
        }
    }, 90);
}

function calculoEdad()
{
    console.log($("#fecnac").val());
    //var expReg = new RegExp(/([0-2][0-9]|[3][0-1])\-([0][0-9]|[1][0-2])\-[0-9]{4}/);
    var expReg = new RegExp(/[0-9]{4}\-([0][0-9]|[1][0-2])\-([0-2][0-9]|[3][0-1])/);
    console.log((expReg.test($("#fecnac").val())));
    if(expReg.test($("#fecnac").val()))
    {
        var tmp = $("#fecnac").val().split("-");
        var ano_nac = tmp[0];
        var mes_nac = tmp[1];
        var fecact = new Date();
        var ano_act = fecact.getFullYear();
        var mes_act = fecact.getMonth() + 1;
        var edad = (ano_act - ano_nac);

        if(parseInt(mes_nac) > parseInt(mes_act))
        {
            edad--;
        }
        $("#edaemp").val(edad);
    }        
}

function convertirMayuscula(obj)
{
    $(obj).val(obj.value.toUpperCase());
}

function convertirMinuscula(obj)
{
    $(obj).val(obj.value.toLowerCase());
}

function soloLetras(obj,event)
{
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    var expReg = new RegExp(/[a-zA-Z]/);
    return expReg.test(valor);
}

function soloNumero(obj,event)
{
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    var expReg = new RegExp(/[0-9]/);
    return expReg.test(valor);  
}

function soloRif(obj,event)
{
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    var expReg = new RegExp(/[0-9]/);

    if($(obj).val().length == 8)
    {
        var expReg = new RegExp(/-/);
    }
    else if($(obj).val().length == 10)
    {
        var expReg = new RegExp(/^[0-9]{8}-[0-9]$/);
    }

    return expReg.test(valor);  
}

function soloMayuscula(obj,event)
{
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    var expReg = new RegExp(/[A-Z]/);
    return expReg.test(valor);
}
function soloMinuscula(obj,event)
{
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    var expReg = new RegExp(/[a-z]/);
    return expReg.test(valor);
}

function soloFecha(obj)
{
    var $campo = $(obj);
    console.log($campo.val());
    var valor = String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode);
    console.log($campo.val().length);
    if($campo.val().lentgh)
    //var valor = $campo.val();//+(String.fromCharCode(event.charCode) || String.fromCharCode(event.keyCode));
    //var expReg = new RegExp(/([0-2][0-9]|[3][0-1])\/([0][0-9]|[1][0-2])\/[0-9]{4}/);
    var expReg = new RegExp(/[0-9\/]*/);
    console.log(valor);
    console.log(expReg.test(valor));

    //return expReg.test(valor);
}

function activarDatepicker()
{
    $('.input-group.date').datepicker({
        language: "es",
        orientation: "left",
        autoclose: true,
        todayHighlight: true,
        toggleActive: true
    });
}

function peticionAjax(url , datos , tipo , tipoDato , exito , fallo)
{
    if(typeof(url) == "undefined")
    {
        alert("Falta el parámetro 'URL'");
        return false;
    }
    datos = (typeof(datos) != "undefined")?datos:{};
    tipo = (typeof(tipo) != "undefined")?tipo:"POST";
    tipoDato = (typeof(tipo) != "undefined")?tipoDato:"json";
    exito = (typeof(exito) != "undefined")?exito:function(){};
    fallo = (typeof(fallo) != "undefined")?fallo:function(){};

    $.ajax({
        url: url,
        type: tipo,
        data:datos,
        dataType: tipoDato,
        success:exito,
        error:fallo
    });
}

function abrirAcordeon(id)
{
    var $obj = $("#"+id);
    $.each($obj.find(".panel-collapse"),function(){
        $(this).addClass("in");
    });
}

function cerrarAcordeon(id)
{
    var $obj = $("#"+id);
    $.each($obj.find(".panel-collapse"),function(){
        $(this).removeClass("in");
    });
}

function validarArchivo(archivo) 
{
    var validExts = new Array(".jpg", ".jpeg","png");
    var fileExt = archivo.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    
    if (validExts.indexOf(fileExt) < 0) 
    {
        alert("El Archivo seleccionado es invalido, Las extenciones validas son " + validExts.toString() + ".");
        return false;
    }
    else
    {
        return true;
    }
}


function confirmarEliminar(ruta)
{
    var respuesta = confirm("¿Esta seguro de que quiere eliminar este elemento?");
    if(respuesta)
    {
        document.location.href = ruta;
    }
    else
    {
        return false;
    }
}

function agregarProductoPrincipal(obj)
{
    var $pro_pri = $(obj).parents('.row').eq(0);    
    var nuevapro_pri = $pro_pri.clone();
    if(pro_pri >= 1 && pro_pri < 3)
    {        
        $(obj).addClass('hidden');
        $(nuevapro_pri).find('.hidden').removeClass('hidden');
        $(nuevapro_pri).find('h3').remove();
        $(nuevapro_pri).find('input').val('');
        $(nuevapro_pri).find('select').val(0);
        $(nuevapro_pri).find("#list_per").addClass("hidden");
        $pro_pri.after(nuevapro_pri);
        pro_pri++;
    }
}

function eliminarProductoPrincipal(obj)
{
    if(pro_pri > 1)
    {   
        var $obj = $(obj);
        $obj.parents('.row').eq(0).prev().eq(0).find('.btn-success').removeClass('hidden');
        pro_pri--;
        $obj.parents('.row').eq(0).remove();
    }
}

function agregarDescripcionSectorEco(obj)
{
    var $sec_eco = $(obj).parents('.row').eq(0);    
    var nuevasec_eco = $sec_eco.clone();
    if(sec_eco >= 1 && sec_eco < 2)
    {        
        $(obj).addClass('hidden');
        $(nuevasec_eco).find('.hidden').removeClass('hidden');
        $(nuevasec_eco).find('h3').remove();
        $(nuevasec_eco).find('input').val('');
        $(nuevasec_eco).find('select').val(0);
        $(nuevasec_eco).find("#list_per").addClass("hidden");
        $sec_eco.after(nuevasec_eco);
        sec_eco++;
    }
}

function eliminarDescripcionSectorEco(obj)
{
    if(sec_eco > 1)
    {   
        var $obj = $(obj);
        $obj.parents('.row').eq(0).prev().eq(0).find('.btn-success').removeClass('hidden');
        sec_eco--;
        $obj.parents('.row').eq(0).remove();
    }
}

function mostrarOpciones(elementos)
{
    if($.isArray(elementos))
    {
        $.each(elementos,function(){
            $("."+this).removeClass("hidden");
        });
    }
    else
    {
        $("."+elementos).removeClass("hidden");
    }
    
}

function ocultarOpciones(elementos)
{
    if($.isArray(elementos))
    {
        $.each(elementos,function(){
            $("."+this).addClass("hidden");
        });
    }
    else
    {
        $("."+elementos).addClass("hidden");
    }
    
}

function opciones(obj , elementos)
{
    var $obj = $(obj);
    var val = $obj.find('option:selected').val();
    
    if(
        val.toUpperCase() == "SI" || 
        val.toUpperCase() == "TRUE" || 
        (
            (val.toUpperCase() != "NO" && val.toUpperCase() != "FALSE") &&
            val != 0
        ) 
    )
    {
        mostrarOpciones(elementos);
    }
    else if((val.toUpperCase() == "NO" || val.toUpperCase() == "FALSE") || val == 0)
    {
        ocultarOpciones(elementos);
    }
}

function opcionesInput(obj , elementos , pos)
{
    pos = (typeof(pos) == "undefined")?0:pos;
    var $obj = $(obj);
    var val = $obj.val();
    
    if(val > 0)
    {
        $("."+elementos).eq(pos).removeClass("hidden");
    }
    else
    {
        $("."+elementos).eq(pos).addClass("hidden");
    }
}

function sumar(selector1,selector2,resultado) 
{    
    var val1 = ($("#"+selector1).val() != "")?parseInt($("#"+selector1).val()):0;
    var val2 = ($("#"+selector2).val() != "")?parseInt($("#"+selector2).val()):0;
    $("#"+resultado).val(val1+val2);
}

function cargaMunicipio(obj , selector1 , selector2 , ine)
{
    selector2 = (typeof(selector2) != "undefined")?selector2:false;
    ine = (typeof(ine) != "undefined")?ine:false;
    
    var $contenedor = $(obj).parents(".row").eq(0);    
    var url = base_url+"unidadeseconomica/ajaxListarMunicipios/"+obj.value+"/"+ine;
    cargando();
    var exito = function(datos){           
        
        $contenedor.find("#"+selector1).empty();
        $contenedor.find("#"+selector1).append("<option value='0'>Seleccionar</option>");

        if(datos.length != 0)
        {
            $.each(datos,function(i,e){
                if(ine)
                {
                    e.descripcion = e.descripcion.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $contenedor.find("#"+selector1).append("<option value='"+e.cod_mun+"'>"+e.descripcion+"</option>");
                }
                else
                {
                    e.nom_mun = e.nom_mun.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $contenedor.find("#"+selector1).append("<option value='"+e.cod_mun+"'>"+e.nom_mun+"</option>");
                }
            });
            $contenedor.find("#"+selector1).removeAttr("disabled");
            $contenedor.find("#"+selector1).focus();
        }
        else
        {                         
            if (selector2)
            {
                $contenedor.find("#"+selector2).val(0);
                $contenedor.find("#"+selector2).attr("disabled","disabled");
            }
            $contenedor.find("#"+selector1).attr("disabled","disabled");
        }
    };
    peticionAjax(url , {}, "POST" , "json" , exito);                
}

function cargaParroquia(obj , selector , selector1 , selector2 , ine)
{
    selector2 = (typeof(selector2) != "undefined")?selector2:false;
    ine = (typeof(ine) != "undefined")?ine:false;
    
    var $contenedor = $(obj).parents(".row").eq(0);
    var cod_ent = $("#"+selector).val();
    var url = base_url+"unidadeseconomica/ajaxListarParroquias/"+cod_ent+"/"+obj.value+"/"+ine;
    cargando();
    var exito = function(datos){            
        $contenedor.find("#"+selector1).empty();
        $contenedor.find("#"+selector1).append("<option value='0'>Seleccionar</option>");            
        
        if(datos.length != 0)
        {
            $.each(datos,function(i,e){
                if(ine)
                {
                    e.descripcion = e.descripcion.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $contenedor.find("#"+selector1).append("<option value='"+e.cod_parrq+"'>"+e.descripcion+"</option>");
                }
                else
                {
                    e.nom_par = e.nom_par.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $contenedor.find("#"+selector1).append("<option value='"+e.cod_prr+"'>"+e.nom_par+"</option>");
                }
            });
            $contenedor.find("#"+selector1).removeAttr("disabled");                                
            $contenedor.find("#"+selector1).focus();
        }
        else
        {            
            if(selector2)
            {
                $contenedor.find("#"+selector2).val(0);
                $contenedor.find("#"+selector2).attr("disabled","disabled");
            }
            $contenedor.find("#"+selector1).attr("disabled","disabled");
        }

    };
    peticionAjax(url , {}, "POST" , "json" , exito);
}

function cargaCentroPoblado(obj , selector1 , selector2 , municipio , entidad)
{
    selector2 = (typeof(selector2) != "undefined")?selector2:false;
    var $contenedor = $(obj).parents(".row").eq(0);
    var cod_ent = $("#"+entidad).val();
    var cod_mun = $("#"+municipio).val();
    
    var url = base_url+"unidadeseconomica/ajaxListarCentroPoblado/"+cod_ent+"/"+cod_mun+"/"+obj.value;
    cargando();
    var exito = function(datos){            
        $contenedor.find("#"+selector1).empty();
        $contenedor.find("#"+selector1).append("<option value='0'>Seleccionar</option>");            
        
        if(datos.length != 0)
        {
            $.each(datos,function(i,e){
                e.descripcion = e.descripcion.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
                $contenedor.find("#"+selector1).append("<option value='"+e.cod_parrq+"'>"+e.descripcion+"</option>");
            });
            $contenedor.find("#"+selector1).removeAttr("disabled");                                
            $contenedor.find("#"+selector1).focus();
        }
        else
        {            
            if(selector2)
            {
                $contenedor.find("#"+selector2).val(0);
                $contenedor.find("#"+selector2).attr("disabled","disabled");
            }
            $contenedor.find("#"+selector1).attr("disabled","disabled");
        }

    };
    peticionAjax(url , {}, "POST" , "json" , exito);
}


function verificarTotalNacionalidad()
{
    var total = $("#total").val();    
    var suma = parseInt($("#tot_ven").val()) ;
    suma += parseInt($("#tot_col_per_ocu").val());
    suma += parseInt($("#tot_otr_per_ocu").val());
    
    if(total != suma)
    {
        $(".total_nacionalidad").removeClass("hidden");
        $("button[type='submit']").attr("disabled","disabled");
    }
    else
    {
        $(".total_nacionalidad").addClass("hidden");
        $("button[type='submit']").removeAttr("disabled");
    }
}

function verificarTotalArea()
{
    var total = $("#total").val();    
    
    var suma = parseInt($("#tot_ger_per_ocu").val());
    suma +=parseInt($("#tot_adm_per_ocu").val());
    suma +=parseInt($("#tot_pro_per_ocu").val());
    suma +=parseInt($("#tot_ven_per_ocu").val());
    suma +=parseInt($("#tot_com_per_ocu").val());
    suma +=parseInt($("#cap_max_per_ocu").val());
    
    if(total != suma)
    {
        $(".total_area").removeClass("hidden");
        $("button[type='submit']").attr("disabled","disabled");
    }
    else
    {
        $(".total_area").addClass("hidden");
        $("button[type='submit']").removeAttr("disabled");
    }
}