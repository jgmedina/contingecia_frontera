<?php
    $acl = unserialize(Session::get('acl'));                
    $cont_perm = Session::get("cont_perm");
    
    function validarControlaodres($controladores)
    {
        $cont_perm = Session::get("cont_perm");
        if(is_array($controladores))
        {
            $cont = 0;
            foreach ($controladores as $value) 
            {
                if(in_array($value, $cont_perm))
                {
                    $cont++;
                }
            }        
            return ($cont > 0 )?true:false;
        }
        else 
        {
            if(in_array($controladores, $cont_perm))
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
    }
?>
<ul class="nav navbar-nav navbar-left">            
    <ul class="nav navbar-nav top-nav">
        <li class="" >
            <a href="<?=BASE_URL;?>unidadeseconomica/actionAgregar" >
                <span class="glyphicon glyphicon-home"></span>&nbsp;Inicio
            </a>
        </li>
    </ul>
    <ul class="nav navbar-nav top-nav">
        <?php 
        $controladores = Array("usuarios","roles","permisos","formularios");
        if(validarControlaodres($controladores))
        {?>
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="glyphicon glyphicon-"></span>&nbsp;Administrador
                <!--<span class="badge badge-info">&nbsp;5</span>-->
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
            <?php 
                if(validarControlaodres("usuarios"))
                {?>
                <li>
                    <a href="<?=BASE_URL . 'usuarios';?>">
                        <span class="glyphicon glyphicon-user"></span>&nbsp;Usuarios
                    </a>
                </li> 
            <?php
            }?>
            <?php 
                if(validarControlaodres("roles"))
                {?>
                <li>
                    <a href="<?=BASE_URL . 'roles';?>">
                        <span class="glyphicon glyphicon-asterisk"></span>&nbsp;Roles
                    </a>
                </li> 
            <?php
            }?>
            <?php 
                if(validarControlaodres("permisos"))
                {?>
                <li>
                    <a href="<?=BASE_URL . 'permisos';?>">
                        <span class="glyphicon glyphicon-cog"></span>&nbsp;Permisos 
                    </a>
                </li> 
            <?php
            }?>
            <?php 
                if(validarControlaodres("formularios"))
                {?>
                <li>
                    <a href="<?=BASE_URL . 'formularios';?>">
                        <span class="glyphicon glyphicon-list"></span>&nbsp;Formularios
                    </a>
                </li> 
            <?php
            }?>
            </ul>
        </li>
        <?php
        }?>                        
    </ul>
    
    <?php 
    if(validarControlaodres("unidadeseconomica"))
    {?>
    <ul class="nav navbar-nav top-nav">
        <li class="" >
            <a href="<?=BASE_URL;?>unidadeseconomica/actionAgregar" >
                <span class="glyphicon glyphicon-list-alt"></span>&nbsp;Unidades Económicas
            </a>
        </li>
    </ul>
    <?php 
    }?>
    
    <?php 
    if(validarControlaodres("unidadesmedidas"))
    {?>
    <ul class="nav navbar-nav top-nav">
        <li class="" >
            <a href="<?=BASE_URL;?>unidadesmedidas/" >
                <span class="glyphicon glyphicon-list-alt"></span>&nbsp;Unidades de Medida
            </a>
        </li>
</ul>
    <?php 
    }?>
</ul>

<ul class="nav navbar-nav navbar-right">            
    <ul class="nav navbar-nav top-nav">
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="glyphicon glyphicon-user"></span>&nbsp;<?= Session::get("ape_usu")." ".Session::get("nom_usu");?>
                <!--<span class="badge badge-info">&nbsp;5</span>-->
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <?php 
                $permKey = 'contrasena_index';
                if($acl->hasPermission($permKey))
                {?>
                <li>
                    <a href="<?=BASE_URL . 'contrasena/';?>">
                        <span class="glyphicon glyphicon-edit"></span>&nbsp;Cambiar Contraseña
                    </a>
                </li> 
                <?php
                }?>
                
                <li>
                    <a href="<?=BASE_URL . 'salir/';?>">
                        <span class="glyphicon glyphicon-off"></span>&nbsp;Cerrar Sessión
                    </a>
                </li> 
            </ul>
        </li>
    </ul>
</ul>