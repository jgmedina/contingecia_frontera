<?php
class formulariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {        
        $explorador = new Explorador();
        
        $dir = 'views/';
        $carpeta = Array();
        
        if(is_dir($dir))
        {
            /*Abrimos directorio*/
            if ($dh = opendir($dir)) 
            {
                /*Recorremos directorio*/
                while (($file = readdir($dh)) !== false) 
                {
                    if($file[0] != "." && $file != "layout" && $file != "denegado" && $file != "formularios")
                    {
                        $carpeta[$file]["nombre"] = $file;
                        $carpeta[$file]["max_elemento"] = 0;
                        if ($dh2 = opendir($dir.$file)) 
                        {
                            $carpeta[$file]["datos"] = Array();
                            while (($arc = readdir($dh2)) !== false) 
                            {
                                if(is_file($dir.$file.'/'. $arc))
                                {                                                                        
                                    $campos = $explorador->leer_archivo($dir.$file.'/'. $arc);                                                                        
                                    array_push($carpeta[$file]["datos"], Array("archivo" => str_replace(".phtml", "", $arc) , "elementos" => count((array)$campos)));
                                    if(count((array)$campos) > $carpeta[$file]["max_elemento"])
                                    {
                                        $carpeta[$file]["max_elemento"] = count((array)$campos) ;
                                    }
                                }
                            }
                        }
                    }                   
                }
                closedir($dh);
            }
        }
        
        $this->_view->elementos = $carpeta;        
        $this->_view->render('lista');
    }       
    
    public function actionEditar($carpeta , $archivo)
    {
        if(count($this->data["formulario"]) > 0)
        {           
            $campo = $this->data["formulario"]["id_cam"];            
            $controlador = $this->data["formulario"]["controlador"];
            $metodo = $this->data["formulario"]["metodo"];
            
            unset($this->data["formulario"]["controlador"]);
            unset($this->data["formulario"]["metodo"]);
            unset($this->data["formulario"]["id_cam"]);
            $datos = $this->data["formulario"];
            
            if(is_array($datos))
            {                  
                $contador = 0;
                ValidadoresCampo::eliminarValidacionesCampo($campo,$controlador,$metodo);                
                
                foreach ($datos as $index => $val) 
                {
                    if(!empty($val["val"]) || strlen($val["val"]) > 0)
                    {
                        if(isset($val["val"]) && isset($val["msg"]) && (!empty($val["val"]) || strlen($val["val"]) > 0) && !empty($val["msg"]))
                        {      
                            switch ($index) 
                            {
                                case "regex":
                                    $index = "data-validation-regex-regex";
                                    break;                       
                                case "callback":
                                    $index = "data-validation-callback-callback";
                                    break;                       
                                case "ajax":
                                    $index = "data-validation-ajax-ajax";
                                    break;                       
                                case "maxchecked":
                                    $index = "data-validation-maxchecked-maxchecked";
                                    break;                       
                                case "minchecked":
                                    $index = "data-validation-minchecked-minchecked";
                                    break;                                               
                            }       
                            
                            if(Validaciones::agregarValidaciones($carpeta , $archivo, $campo , $val["val"] , $val["msg"] , $index))
                            {
                                $contador++;
                            }
                        }
                    }
                    else
                    {
                        unset($val);
                    }
                }
            }  
            
            if($contador > 0)
            {            
                $this->mensaje(-1, "A" , "", "Se valido el campo correctamente" , "alert-warning") ;
            }        
        }
        
        $explorador = new Explorador();
        $ruta_archivo = "views/$carpeta/$archivo.phtml";                
        $elementos = (Array)$explorador->leer_archivo($ruta_archivo);

        $tipos_datos = Array();
        if(count($elementos) > 0)
        {
            foreach ($elementos as $key => $value) 
            {            
                $value->validaciones = Array();                        
                $Validaciones = OrigenCampo::consultarCampo($key);
                if($Validaciones)
                {                
                    foreach ($Validaciones as $val) 
                    {                    
                        array_push($value->validaciones , (object) Array("tip" => $val->cod_att_val,"val" => $val->val_att_val_cam,"msg" =>$val->val_msg_val_cam));                
                    }                
                }                        
            }
        }

        $this->_view->controlador = $carpeta;        
        $this->_view->metodo = $archivo;        
        $this->_view->elementos = $elementos;                    
        $this->_view->render('editar');        
    }
    
    public function ajaxConsultarCampo($nom_campo)
    {
        $validaciones = OrigenCampo::consultarCampo($nom_campo);
        $resultado = Array();
        if($validaciones)
        {                
            foreach ($validaciones as $val) 
            {                    
                array_push($resultado , (object) Array("tip" => $val->cod_att_val,"val" => $val->val_att_val_cam,"msg" =>$val->val_msg_val_cam));                
            }                
        }        
        print json_encode($resultado);
    }
}
?>
