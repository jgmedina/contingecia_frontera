<?php
class indexController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {   
        if(Session::get("msg_error"))
        {          
            $this->_view->msg_error = Session::get("msg_error"); 
            Session::destroy();                
            $this->_view->render('index');
            exit();
        }
        
        if (isset($_REQUEST["log_usu"]) && isset($_REQUEST["pas_usu"])) 
        {
            $log_usu = trim(strtolower($_REQUEST["log_usu"]));
            $pas_usu = md5(trim($_REQUEST["pas_usu"]));            
            $datos = Login::valida_acceso($log_usu, $pas_usu);
            if (!$datos) 
            {
                Session::set("msg_error", "No existe el usuario o la contraseña es incorrecta.");
                $this->redireccionar();
            } 
            else 
            {
                $usuario = Usuario::consultarUsuario($datos->id);
                
                Session::set("id_usu", $usuario->id);
                Session::set("nom_usu", $usuario->nom_usu);
                Session::set("ape_usu", $usuario->ape_usu);
              
                if(!Session::get('acl'))
                {
                    $acl = new acl(Session::get('id_usu'));
                    Session::set('acl',  serialize($acl));
                    unset($_REQUEST);
                    
                    $controladores_permitidos = Array();
                    foreach ($acl->perms as $key => $value) 
                    {
                        $tmp = explode("_", $key);
                        $controlador = $tmp[0];
                        if(!in_array($controlador, $controladores_permitidos))
                        {                            
                            array_push($controladores_permitidos, $controlador);
                        }
                    }
                    Session::set('cont_perm',  $controladores_permitidos);
                }
                
                $this->_view->unidades_medidas = UnidadesMedida::listarUnidadesMedidas();
                $this->_view->infraestructuras = Infraestructura::listarInfraestructuras();
                $this->redireccionar('unidadeseconomica/actionAgregar');
                exit();
            }
        } 
        else if (!Session::get('id_usu')) 
        {
            $this->_view->render('index');
        } 
        else 
        {                    
            $this->_view->unidades_medidas = UnidadesMedida::listarUnidadesMedidas();
            $this->_view->infraestructuras = Infraestructura::listarInfraestructuras();
            $this->redireccionar('unidadeseconomica/actionAgregar');
        }
    }           
}
?>
