<?php
class unidadesmedidasController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {
        $this->_view->unidadesmedidas = UnidadesMedida::listarUnidadesMedidas();        
        $this->_view->render('lista');
    }           
    
    public function actionAgregar()
    {                
        if(count($this->data["medida"]) > 0)
        {            
            $this->mensaje(UnidadesMedida::agregarUnidadesMedidas($this->data["medida"]) , "A" , "La Unidad de Medida", "La Unidad de Medida que intenta registrar ya existe." , "alert-warning") ;
        }        
        
        $this->_view->grupos_unidades_medidas = UnidadesMedida::listarGruposUnidadesMedidas();        
        $this->_view->render("agregar");
    }

    public function actionConsultar($id)
    {
        $this->_view->unidadesmedidas = UnidadesMedida::consultarUnidadesMedidas($id);        
        $this->_view->render("consultar");
    }  
    
    public function actionEditar($id)
    {           
        if(count($this->data["medida"]) > 0)
        {
            $this->data["medida"]["id_uni_med"] = $id;            
            $this->mensaje(UnidadesMedida::modificarUnidadesMedidas($this->data["medida"]) , "M", "La Unidad de Medida","Error al realizar la edición","alert-warning");
        }            
        $this->_view->unidadesmedidas = UnidadesMedida::consultarUnidadesMedidas($id);
        $this->_view->render("editar");
    }    
    
    public function actionEliminar($id)
    {   
        $this->mensaje(UnidadesMedida::eliminarUnidadesMedidas($id), "E", "La Unidad de Medida");
        $this->_view->unidadesmedidas = UnidadesMedida::listarUnidadesMedidas();
        $this->_view->render('lista');
    }    
}
?>
