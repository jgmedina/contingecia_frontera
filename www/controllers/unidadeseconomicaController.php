<?php
class unidadeseconomicaController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {
        //$this->_view->render('agregar');
        $this->redireccionar('unidadeseconomica/actionAgregar');
    }           
    
    public function actionAgregar()
    {
        if(
                count($this->data["identificacion"]) > 0 &&
                count($this->data["ubigeo"]) > 0 &&
                //count($this->data["cod_cat"]) > 0 &&
                count($this->data["empresas"]) > 0 &&
                count($this->data["propietario"]) > 0 &&
                count($this->data["propietario_ter"]) > 0 &&
                count($this->data["propietario_inf"]) > 0 &&
                count($this->data["nivel_act"]) > 0 &&
                count($this->data["uni_eco_inf"]) > 0 &&
                count($this->data["sector_economico"]) > 0 &&
                count($this->data["datos_productivos"]) > 0 &&
                count($this->data["productos"]) > 0 &&
                count($this->data["materias"]) > 0 &&
                count($this->data["destino"]) > 0 &&
                count($this->data["proveedores"]) > 0 &&
                count($this->data["personal_ocu"]) > 0 &&
                count($this->data["referencias_car"]) > 0 &&
                //count($this->data["recuperacion_pro"]) > 0 &&
                count($this->data["entrevistado"]) > 0 &&
                count($this->data["empadronador"]) > 0 &&
                count($this->data["situaciones_pre"]) > 0 &&
                count($this->data["unidad_eco"]) > 0 
          )
        {                        
            $this->mensaje(UnidadEconomica::agregarUnidadEconomica($this->data) , "A" , "La Planilla", "La Planilla que intenta registrar ya existe." , "alert-warning") ;
        }
        
        $this->_view->entidadesIne = UnidadEconomica::listarEntidades(TRUE);
        $this->_view->entidades = UnidadEconomica::listarEntidades();
        
        $this->_view->unidades_medidas = UnidadesMedida::listarUnidadesMedidas();
        $this->_view->infraestructuras = Infraestructura::listarInfraestructuras();
        $this->_view->render('agregar');
    }           
    
    public function ajaxListarEntidades($ine = false)
    {        
        print json_encode(UnidadEconomica::listarEntidades());
    }
    
    public function ajaxListarMunicipios($id_ent , $ine = false)
    {        
        print json_encode(UnidadEconomica::listarMunicipios($id_ent,$ine));
    }
    
    public function ajaxListarParroquias($cod_ent,$id_mun,$ine = false)
    {        
        print json_encode(UnidadEconomica::listarParroquias($cod_ent,$id_mun,$ine));
    }
    
    public function ajaxListarCentroPoblado($cod_ent , $cod_mun , $cod_par)
    {        
        print json_encode(UnidadEconomica::listarCentroPoblado($cod_ent , $cod_mun , $cod_par));
    }
}
?>
